using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


[RequireComponent(typeof(AudioSource))]

public class BombSpawner : MonoBehaviour
{
    [SerializeField]
    private Bomb blackBombPrefab; 

    [SerializeField]
    private Bomb silverBombPrefab;
    [SerializeField]
    private Bomb redBombPrefab;
    [SerializeField]
    private Bomb greenBombPrefab;
    [SerializeField]
    private Bomb purpleBombPrefab;
    [SerializeField]
    private Bomb yellowBombPrefab;

    private List<Bomb> activeBombs = new List<Bomb>();
    private bool BombsActive => activeBombs.Count > 0;

    [SerializeField]
    private int initialBombs = 10;
    private int bombsLeft;           // to spawn
    private int bombsUsed;           // spawned (UseBomb)

    private int bombCountdownRemaining;       // reduced by spawning worm sections, use bomb on zero
    private int bombCountdownStart = 100;
    private bool bombCountdownStarted = false;    // true when first worm hits ground

    [SerializeField]
    private int sectionsPerBomb = 10;               // number of worm sections to earn a new bomb
    private int totalWormSectionsSpawned = 0;       // total worm sections spawned
    private int totalWormSectionsAlive = 0;         // total worm sections not yet 'dead'
    private int totalWormSectionsDead = 0;          // total worm sections
    private int totalWormSectionsScattered = 0;     // total worm sections
    private int wormSectionsInHand = 0;             // bombed worm sections in 'wallet' to trade for bombs
    private int bombsGained = 0;                    // by bombing worms

    private int scoreFactor = 10;
    public int Score => (totalWormSectionsScattered * scoreFactor) / (bombsUsed > 0 ? bombsUsed : 1);

    [SerializeField]
    private AudioClip noBombAudio;
    [SerializeField]
    private AudioClip bombSetAudio;             // when placed
    [SerializeField]
    private AudioClip bombLostAudio;            // countdown expired
    [SerializeField]
    private AudioClip gameOverAudio;

    [SerializeField]
    private ParticleSystem spawnParticles;
    [SerializeField]
    private ParticleSystem smokeParticles;

    [SerializeField]
    private ParticleSystem popParticles;        // prefab
    private float popDestroyDelay = 10f;        // destroy pop particles

    private int spawnCooldownTime = 6;          // seconds
    private bool coolingDown;

    private float destroyDelay = 3f;        // to allow particles / audio to play out

    private float bombYOffset = 1.4f;
    private float silverBombYOffset = 1.75f;

    private float bombSpawnTime = 1f;

    private bool gameOver = false;


    private AudioSource audioSource;


    private void Start()
    {
        LeanTween.init(6400);

        audioSource = GetComponent<AudioSource>();

        bombsLeft = initialBombs;
        bombsUsed = 0;
        GameEvents.OnBombsLeftUpdated?.Invoke(bombsLeft, bombsUsed);

        ResetBombCountdown();

        totalWormSectionsSpawned = 0;
        totalWormSectionsAlive = 0;
        totalWormSectionsDead = 0;
        totalWormSectionsScattered = 0;
        wormSectionsInHand = 0;

        GameEvents.OnWormSectionsUpdated?.Invoke(totalWormSectionsAlive, totalWormSectionsDead,
                                            totalWormSectionsScattered, wormSectionsInHand, sectionsPerBomb);

        bombsGained = 0;
        GameEvents.OnBombsGainedUpdated?.Invoke(bombsGained);

        coolingDown = false;
        GameEvents.OnBombCooldown?.Invoke(coolingDown);
    }

    private void OnEnable()
    {
        GameEvents.OnWormHitGround += OnWormHitGround;              // first worm

        GameEvents.OnBombActivated += OnBombActivated;
        GameEvents.OnBombDetonated += OnBombDetonated;
        GameEvents.OnBombFinished += OnBombFinished;

        GameEvents.OnWormDead += OnWormDead;
        GameEvents.OnWormScattered += OnWormScattered;

        GameEvents.OnWormSectionSpawned += OnWormSectionSpawned;
        GameEvents.OnWormSectionPopped += OnWormSectionPopped;
        GameEvents.OnTailSectionScattered += OnTailSectionScattered;
    }

    private void OnDisable()
    {
        GameEvents.OnWormHitGround -= OnWormHitGround;              // first worm

        GameEvents.OnBombActivated -= OnBombActivated;
        GameEvents.OnBombDetonated -= OnBombDetonated;
        GameEvents.OnBombFinished -= OnBombFinished;

        GameEvents.OnWormDead -= OnWormDead;
        GameEvents.OnWormScattered -= OnWormScattered;

        GameEvents.OnWormSectionSpawned -= OnWormSectionSpawned;
        GameEvents.OnWormSectionPopped -= OnWormSectionPopped;
        GameEvents.OnTailSectionScattered -= OnTailSectionScattered;
    }


    private void OnBombDetonated(Bomb bomb)
    {
        if (smokeParticles != null)
        {
            smokeParticles.transform.position = new Vector3(bomb.transform.position.x, smokeParticles.transform.position.y, bomb.transform.position.z);
            smokeParticles.Play();
        }
    }

    private void OnBombFinished(Bomb bomb)
    {
        activeBombs.Remove(bomb);
        Destroy(bomb.gameObject, destroyDelay);

        if (bombsLeft == 0 && !gameOver && !BombsActive)         // last bomb finished - no bombs left!
        {
            GameOver();
        }
    }


    // bomb activated (spawned) by player touch on
    private void OnBombActivated(Vector3 position)
    {
        if (gameOver)
            return;

        var bombPosition = new Vector3(position.x, position.y + bombYOffset, position.z);

        var newBomb = SpawnBomb(bombPosition, true, Bomb.BombColourType.Default);

        if (newBomb == null)        // no more bombs left!
            return;

        GameEvents.OnBombSpawned?.Invoke(newBomb);

        UseBomb(false);
        StartCoroutine(CooldownTimer());
        ResetBombCountdown();
    }


    private Bomb SpawnBomb(Vector3 position, bool checkBombsLeft, Bomb.BombColourType bombColour = Bomb.BombColourType.Default)
    {
        if (spawnParticles != null)
        {
            spawnParticles.transform.position = new Vector3(position.x, spawnParticles.transform.position.y, position.z);
            spawnParticles.Play();
        }

        if (checkBombsLeft)
        {
            if (bombsLeft == 0 || coolingDown)
            {
                if (noBombAudio != null)
                {
                    audioSource.clip = noBombAudio;
                    audioSource.Play();
                }
                return null;
            }
        }

        if (bombSetAudio != null)
        {
            audioSource.clip = bombSetAudio;
            audioSource.Play();
        }

        // fuse countdown starts immediately
        Bomb prefab;

        switch (bombColour)
        {
            case Bomb.BombColourType.Default:
            default:
                prefab = blackBombPrefab;
                break;

            case Bomb.BombColourType.Silver:
                prefab = silverBombPrefab;
                break;

            case Bomb.BombColourType.Purple:
                prefab = purpleBombPrefab;
                break;

            case Bomb.BombColourType.Yellow:
                prefab = yellowBombPrefab;
                break;

            case Bomb.BombColourType.Red:
                prefab = redBombPrefab;
                break;

            case Bomb.BombColourType.Green:
                prefab = greenBombPrefab;
                break;
        }

        var bombObj = Instantiate(prefab.gameObject, transform);
        var bomb = bombObj.GetComponent<Bomb>();

        if (bombColour != Bomb.BombColourType.Default)
        {
            // drop down to ground
            bombObj.transform.position = position;
            var targetPosition = new Vector3(position.x, silverBombYOffset, position.z);
            LeanTween.move(bombObj, targetPosition, bombSpawnTime)
                                        .setEaseInCirc();
        }
        else
        {
            bombObj.transform.position = new Vector3(position.x, bombYOffset, position.z);
        }

        var startScale = bombObj.transform.localScale;

        bombObj.transform.localScale = Vector3.zero;
        LeanTween.scale(bombObj, startScale, bombSpawnTime)
                    .setEaseOutElastic();

        activeBombs.Add(bomb); 
        return bomb;
    }


    private void OnWormSectionSpawned(WormSection3D worm)
    {
        if (bombCountdownRemaining <= 0 || bombsLeft < 1)
            return;

        totalWormSectionsSpawned++;

        totalWormSectionsAlive++;

        if (bombCountdownStarted)       // when first worm hits ground
        {
            bombCountdownRemaining--;

            if (bombCountdownRemaining <= 0)
            {
                UseBomb(true);          // bomb lost
                ResetBombCountdown();

                if (bombLostAudio != null)
                {
                    audioSource.clip = bombLostAudio;
                    audioSource.Play();
                }

                GameEvents.OnBombCountdownExpired?.Invoke();
            }
            else
                GameEvents.OnBombCountdownChanged?.Invoke(bombCountdownRemaining, false);
        }

        GameEvents.OnWormSectionsUpdated?.Invoke(totalWormSectionsAlive, totalWormSectionsDead, totalWormSectionsScattered, wormSectionsInHand, sectionsPerBomb);
    }

    // spawn a silver bomb!
    private void OnTailSectionScattered(Vector3 position, Bomb.BombColourType colourType)
    {
        if (colourType != Bomb.BombColourType.Default)
        {
            var tailBomb = SpawnBomb(position, false, colourType);
            GameEvents.OnBombSpawned?.Invoke(tailBomb);
        }
    }

    private void OnWormSectionPopped(WormSection3D section, Vector3 position)
    {
        var popParticles = Instantiate(this.popParticles, transform);
        popParticles.transform.position = position;
        popParticles.Play();

        Destroy(popParticles.gameObject, popDestroyDelay);
    }

    private void ResetBombCountdown()
    {
        bombCountdownRemaining = bombCountdownStart;
        GameEvents.OnBombCountdownChanged?.Invoke(bombCountdownRemaining, true);
    }

    private void OnWormHitGround(Worm3D worm)
    {
        bombCountdownStarted = true;
    }

    private void OnWormDead(Worm3D worm)
    {
        totalWormSectionsAlive -= worm.SectionCount;         // total
        totalWormSectionsDead += worm.SectionCount;         // total
        GameEvents.OnWormSectionsUpdated?.Invoke(totalWormSectionsAlive, totalWormSectionsDead, totalWormSectionsScattered, wormSectionsInHand, sectionsPerBomb);
    }

    private void OnWormScattered(Worm3D worm)
    {
        if (gameOver)
            return;

        //Debug.Log($"OnWormScattered: wormSections = {worm.SectionCount}");

        totalWormSectionsDead -= worm.SectionCount;
        totalWormSectionsScattered += worm.SectionCount;         // total
        wormSectionsInHand += worm.SectionCount;             // to trade for bombs (see AddBombs below)

        int bombsToTrade = (int)(wormSectionsInHand / sectionsPerBomb);        // bombs to buy

        if (bombsToTrade > 0)           // enough sections to trade for bomb(s)
            AddBombs(bombsToTrade);     // deducts worm sections

        //Debug.Log($"OnWormScattered: totalWormSectionsScattered = {totalWormSectionsScattered} bombsUsed = {bombsUsed} Score = {Score}");
        GameEvents.OnWormSectionsUpdated?.Invoke(totalWormSectionsAlive, totalWormSectionsDead, totalWormSectionsScattered, wormSectionsInHand, sectionsPerBomb);
    }


    // bomb used either by player placement or
    // bomb countdown expiry
    private void UseBomb(bool bombLost)
    {
        if (gameOver)
            return;

        if (bombsLeft <= 0)
        {
            Debug.LogError("UseBomb: no bombs left!");
            return;
        }

        bombsUsed++;

        bombsLeft--;
        GameEvents.OnBombsLeftUpdated?.Invoke(bombsLeft, bombsUsed);

        if (bombLost)       // bomb not detonated (lost from countdown expiring)
        {
            //Debug.Log($"UseBomb: totalWormSectionsScattered = {totalWormSectionsScattered} bombsUsed = {bombsUsed} Score = {Score}");

            if (bombsLeft == 0 && !BombsActive)
            {
                GameOver();
            }
        }
    }

    private void GameOver()
    {
        if (gameOver)
            return;

        gameOver = true;
        GameEvents.OnGameOver?.Invoke(Score);

        if (gameOverAudio != null)
        {
            audioSource.clip = gameOverAudio;
            audioSource.Play();
        }
    }

    private void AddBombs(int bombsToTrade)
    {
        bombsGained += bombsToTrade;
        GameEvents.OnBombsGainedUpdated?.Invoke(bombsGained);

        bombsLeft += bombsToTrade;
        GameEvents.OnBombsLeftUpdated?.Invoke(bombsLeft, bombsUsed);

        // deduct 'spent' sections
        wormSectionsInHand -= (bombsToTrade * sectionsPerBomb);

        //Debug.Log($"AddBombs: bombsToTrade = {bombsToTrade} bombsLeft = {bombsLeft} wormSectionsToTrade = {wormSectionsInHand}");
    }

    private IEnumerator CooldownTimer()
    {
        coolingDown = true;
        GameEvents.OnBombCooldown?.Invoke(coolingDown);

        int timeRemaining = spawnCooldownTime;

        while (timeRemaining > 0)
        {
            GameEvents.OnBombCooldownTime?.Invoke(timeRemaining);

            yield return new WaitForSecondsRealtime(1f);
            timeRemaining--;
        }

        coolingDown = false;
        GameEvents.OnBombCooldown?.Invoke(coolingDown);
    }
}

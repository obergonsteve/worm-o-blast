﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

/// <summary>
/// Fuse timer for bomb
/// </summary>
public class TimerUI : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI timeRemaining;

    [SerializeField]
    private bool faceCamera = false;            // only needed for 3D / rotating objects / moving camera
    [SerializeField]
    private bool fixedYRotation = true;         // don't face camera on Y axis

    private Camera cameraToLookAt;


    private void Start()
    {
        cameraToLookAt = Camera.main;
    }

    private void Update()
    {
        // keep canvas facing camera
        if (faceCamera)
        {
            if (fixedYRotation)
            {
                var cameraXZ = new Vector3(cameraToLookAt.transform.position.x, transform.position.y, cameraToLookAt.transform.position.z);
                transform.rotation = Quaternion.LookRotation(transform.position - cameraXZ);
            }
            else
            {
                transform.rotation = Quaternion.LookRotation(transform.position - cameraToLookAt.transform.position);
            }
        }
    }

    public void SetTime(int time)
    {
        if (time <= 0)
            timeRemaining.text = "";
        else
            timeRemaining.text = time.ToString();
    }
}

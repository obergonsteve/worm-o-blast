﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// General purpose script for in-game character
/// stats - eg. health (bar), name, etc.
/// </summary>
public class StatsUI : MonoBehaviour
{
    [SerializeField]
    private Text characterName;

    [SerializeField]
    private Image healthBar;

    [SerializeField]
    private float fillTime;

    [SerializeField]
    private bool faceCamera = false;        // only needed for 3D / rotating objects / moving camera
    private Camera mainCam;



    private void Start()
    {
        mainCam = Camera.main;
    }

    private void Update()
    {
        // keep canvas facing camera
        if (faceCamera)
            transform.rotation = Quaternion.LookRotation(transform.position - mainCam.transform.position);
    }

    public void SetName(string name)
    {
        characterName.text = name.ToUpper();
    }

    // tween healthbar to currentHealth value
    // healthBar 'Filled' image constrained to 0 - 1
    public void UpdateHealthBar(float maxHealth, float currentHealth)
    {
        if (maxHealth <= 0)
        {
            Debug.LogError("UpdateHealth: invalid maxHealth!!");
            return;
        }

        // normalise currentHealth (0 - 1)
        float newHealthNorm = currentHealth / maxHealth;

        LeanTween.value(gameObject, healthBar.fillAmount, newHealthNorm, fillTime)
                        .setEaseLinear()
                        .setOnUpdate((float health) =>
                        {
                            healthBar.fillAmount = health;
                        });
    }
}

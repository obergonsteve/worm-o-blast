using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class GameManager : MonoBehaviour
{
    private int bucketMovesPerTurn = 1;
    private int bucketMovesLeft = 0;

    [SerializeField]
    private List<Worm> worms = new List<Worm>();

    private bool wormsFrozen = false;

    //private float gameOverDelay = 5f;

    [SerializeField]
    private int gameTime = 20;         // seconds
    private int gameTimeRemaining = 0;

    public enum GameStatus
    {
        Frozen,     // waiting for player touch off/release (made their move)
        InPlay,     // falling
        GameOver    // all fallen
    }

    public GameStatus CurrentStatus { get; private set; }


    private void OnEnable()
    {
        WormEvents.OnTouchOn += OnTouchOn;
        WormEvents.OnTouchMove += OnTouchMove;
        WormEvents.OnTouchOff += OnTouchOff;

        //WormEvents.OnWormEnterBucket += OnWormEnterBucket;
        //WormEvents.OnWormExitBucket += OnWormExitBucket;
        //WormEvents.OnWormHitFloor += OnWormHitFloor;
    }

    private void OnDisable()
    {
        WormEvents.OnTouchOn -= OnTouchOn;
        WormEvents.OnTouchMove -= OnTouchMove;
        WormEvents.OnTouchOff -= OnTouchOff;

        //WormEvents.OnWormEnterBucket -= OnWormEnterBucket;
        //WormEvents.OnWormExitBucket -= OnWormExitBucket;
        //WormEvents.OnWormHitFloor -= OnWormHitFloor;
    }

   

    private void Start()
    {
        StartCoroutine(GameCountDown());

        bucketMovesLeft = bucketMovesPerTurn;
        //Freeze();
    }

    //// TODO: better way than in an Update??
    //private void FixedUpdate()
    //{
    //    if (AllWormsStopped)
    //        SetStatus(GameStatus.GameOver);
    //}

    // freeze worms while bucket is being moved
    private void OnTouchOn(Vector2 screenPosition, int touchCount)
    {
        if (wormsFrozen || bucketMovesLeft <= 0)
            return;

        //Freeze();
    }

    private IEnumerator GameCountDown()
    {
        gameTimeRemaining = gameTime;

        while (gameTimeRemaining > 0)
        {
            yield return new WaitForSecondsRealtime(1f);

            WormEvents.OnGameCountDown?.Invoke(gameTimeRemaining);

            gameTimeRemaining--;
        }

        WormEvents.OnGameCountDown?.Invoke(gameTimeRemaining);      // zero
        SetStatus(GameStatus.GameOver);
    }

    //private void Freeze()
    //{
    //    wormsFrozen = true;
    //    WormEvents.OnFreezeWorms?.Invoke(wormsFrozen, bucketMovesLeft);
    //    SetStatus(GameStatus.Frozen);
    //}

    //private void UnFreeze()
    //{
    //    wormsFrozen = false;
    //    WormEvents.OnFreezeWorms?.Invoke(wormsFrozen, bucketMovesLeft);
    //    SetStatus(GameStatus.InPlay);
    //}

    private void OnTouchMove(Vector2 screenPosition, int touchCount, Vector2 startScreenPosition, Vector2 lastTouchPosition, TimeSpan timeFromStart)
    {
        if (!wormsFrozen || bucketMovesLeft <= 0)
            return;

        if (touchCount == 1)
            WormEvents.OnBucketMove?.Invoke(screenPosition, lastTouchPosition);
        else if (touchCount == 2)
            WormEvents.OnObstaclesMove?.Invoke(screenPosition, lastTouchPosition);
    }

    // reduce bucket moves left and un-freeze worms
    private void OnTouchOff(Vector2 screenPosition, int touchCount, Vector2 startScreenPosition, TimeSpan timeFromStart)
    {
        if (!wormsFrozen)
            return;

        bucketMovesLeft--;
        //UnFreeze();
    }


    //private void OnWormEnterBucket(Worm worm, List<Worm> wormsInBucket)
    //{
    //    if (AllWormsStopped)
    //        SetStatus(GameStatus.GameOver);
    //}

    //private void OnWormExitBucket(Worm worm, List<Worm> wormsInBucket)
    //{
    //    SetStatus(GameStatus.InPlay);
    //}

    //private void OnWormHitFloor(Worm worm, List<Worm> wormsOnFloor)
    //{
    //    if (AllWormsStopped)
    //        SetStatus(GameStatus.GameOver);
    //}

    private void SetStatus(GameStatus newStatus)
    {
        if (CurrentStatus == newStatus)
            return;

        CurrentStatus = newStatus;

        switch (newStatus)
        {
            case GameStatus.Frozen:
                break;

            case GameStatus.InPlay:
                break;

            case GameStatus.GameOver:
                //StartCoroutine(ResetGame());
                ResetGame();
                break;
        }

        WormEvents.OnGameStatusChanged?.Invoke(newStatus);
    }

    //public bool AllWormsStopped
    //{
    //    get
    //    {
    //        foreach (var worm in worms)
    //        {
    //            if (!worm.HasStopped)
    //                return false;
    //        }

    //        return true;
    //    }
    //}

    //private IEnumerator ResetGame()
    private void ResetGame()
    {
        //yield return new WaitForSeconds(gameOverDelay);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}

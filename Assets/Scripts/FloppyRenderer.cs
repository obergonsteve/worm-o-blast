using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class FloppyRenderer : MonoBehaviour
{
    [SerializeField]
    private LineRenderer lineRenderer;

    [SerializeField]
    private List<Transform> points;

    //private AudioSource squelchAudio;


    void Start()
    {
        //squelchAudio = GetComponent<AudioSource>();
        lineRenderer.positionCount = points.Count;
    }

    void Update()
    {
        DrawLine();
    }


    //private void OnCollisionEnter2D(Collision2D collision)
    //{
    //    squelchAudio.Play();
    //}

    // while in editor only
    private void OnDrawGizmos()
    {
        lineRenderer.positionCount = points.Count;
        DrawLine();
    }

    private void DrawLine()
    {
        // set renderer positions to positions of all of the points 
        lineRenderer.SetPositions(points.Select(p => p.position).ToArray());
    }
}

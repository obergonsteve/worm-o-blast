using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]      // explosion

public class Bomb : MonoBehaviour
{
    public GameObject bombModel;

    public enum BombStatus
    {
        FuseCountdown,
        Detonated,
        Finished    // not used - bomb removed from activeList when bomb force has finished
    };

    public BombStatus CurrentStatus { get; private set; }


    public enum BombColourType
    {
        Default,      // default, hand placed
        Silver,     // bigger, more force
        Purple,     // worm (player) colour
        Yellow,     // worm (player) colour
        Red,        // worm (player) colour
        Green,      // worm (player) colour
    };

    public BombColourType BombColour;

    public ParticleSystem pulseParticles;
    public ParticleSystem sparkParticles;
    public ParticleSystem explosionParticles;

    private float minBombDelay = 0f;             // before fuse countdown
    private float maxBombDelay = 1f;             // before fuse countdown
    private float randomBombDelay => Random.Range(minBombDelay, maxBombDelay);             // before fuse countdown

    private int fuseTime = 3;
    private int timeRemaining = 0;

    private float pulseScale = 1.15f;
    private float pulseTime = 0.075f;

    private Vector3 startScale;

    public TimerUI timerUI;

    public BombForce explosionForce;        // trigger collider adds force on trigger stay

    private AudioSource audioSource;
    public AudioClip tickAudio;
    public AudioClip explosionAudio;


    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        startScale = bombModel.transform.localScale;

        explosionForce.SetBombColour(BombColour);       // for explosion
    }

    private void Start()
    {
        StartCoroutine(FuseCountdown(0)); // randomBombDelay);
    }


    //public void SetColourType(BombColourType bombColour)
    //{
    //    BombColour = bombColour;
    //}

    private IEnumerator FuseCountdown(float delay = 0)
    {
        timeRemaining = fuseTime;
        CurrentStatus = BombStatus.FuseCountdown;

        timerUI.SetTime(0);

        if (delay > 0)
            yield return new WaitForSecondsRealtime(delay);

        while (timeRemaining > 0)
        {
            timerUI.SetTime(timeRemaining);
            Pulse();
            yield return new WaitForSecondsRealtime(1f);
            timeRemaining--;

            yield return null;
        }

        explosionForce.Explode(OnFinish);
        CurrentStatus = BombStatus.Detonated;
        GameEvents.OnBombDetonated?.Invoke(this);

        audioSource.clip = explosionAudio;
        audioSource.Play();

        bombModel.SetActive(false);
        timerUI.gameObject.SetActive(false);

        explosionParticles.Play();
        sparkParticles.Stop();
        pulseParticles.Stop();
    }

    private void OnFinish()
    {
        GameEvents.OnBombFinished?.Invoke(this);
    }

    private void Pulse()
    {
        pulseParticles.Play();

        LeanTween.scale(bombModel, startScale * pulseScale, pulseTime)
                            .setEaseOutCubic()
                            .setOnComplete(() =>
                            {
                                audioSource.clip = tickAudio;
                                audioSource.Play();

                                LeanTween.scale(bombModel, startScale, pulseTime)
                                            .setEaseInCubic();
                            });
    }
}

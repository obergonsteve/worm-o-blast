using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameEvents
{
    // Touch Input

    public delegate void OnTouchOnDelegate(Vector2 screenPosition, int touchCount);
    public static OnTouchOnDelegate OnTouchOn;

    public delegate void OnTouchMoveDelegate(Vector2 screenPosition, int touchCount, Vector2 direction, TimeSpan timeFromStart);
    public static OnTouchMoveDelegate OnTouchMove;

    public delegate void OnTouchOffDelegate(Vector2 screenPosition, int touchCount, Vector2 direction, TimeSpan timeFromStart);
    public static OnTouchOffDelegate OnTouchOff;

    // Bombs

    public delegate void OnBombActivatedDelegate(Vector3 position);
    public static OnBombActivatedDelegate OnBombActivated;              // fuse countdown

    public delegate void OnBombSpawnedDelegate(Bomb bomb);
    public static OnBombSpawnedDelegate OnBombSpawned;              // instantiated

    public delegate void OnBombDetonatedDelegate(Bomb bomb);
    public static OnBombDetonatedDelegate OnBombDetonated;              // fuse countdown expired

    public delegate void OnBombFinishedDelegate(Bomb bomb);
    public static OnBombFinishedDelegate OnBombFinished;              // force completed

    public delegate void OnBombCountdownExpiredDelegate();
    public static OnBombCountdownExpiredDelegate OnBombCountdownExpired;       // countdown expired

    public delegate void OnBombCooldownDelegate(bool coolingDown);
    public static OnBombCooldownDelegate OnBombCooldown;

    public delegate void OnBombCooldownTimeDelegate(int timeRemaining);
    public static OnBombCooldownTimeDelegate OnBombCooldownTime;

    public delegate void OnBombsLeftUpdatedDelegate(int bombsLeft, int bombsUsed);
    public static OnBombsLeftUpdatedDelegate OnBombsLeftUpdated;

    public delegate void OnBombsGainedUpdatedDelegate(int bombsGained);
    public static OnBombsGainedUpdatedDelegate OnBombsGainedUpdated;

    public delegate void OnBombCountdownChangedDelegate(int countdownRemaining, bool reset);
    public static OnBombCountdownChangedDelegate OnBombCountdownChanged;

    public delegate void OnSilverCountdownChangedDelegate(int silverCountdownRemaining);
    public static OnSilverCountdownChangedDelegate OnSilverCountdownChanged;

    // Worms

    public delegate void OnWormSpawnedDelegate(Worm3D worm);
    public static OnWormSpawnedDelegate OnWormSpawned;

    public delegate void OnWormSectionSpawnedDelegate(WormSection3D worm);
    public static OnWormSectionSpawnedDelegate OnWormSectionSpawned;

    public delegate void OnWormHitGroundDelegate(Worm3D worm);      // first worm
    public static OnWormHitGroundDelegate OnWormHitGround;

    public delegate void OnWormDeadDelegate(Worm3D worm);
    public static OnWormDeadDelegate OnWormDead;

    public delegate void OnWormScatteredDelegate(Worm3D worm);
    public static OnWormScatteredDelegate OnWormScattered;

    public delegate void OnWormSectionPoppedDelegate(WormSection3D section, Vector3 position);
    public static OnWormSectionPoppedDelegate OnWormSectionPopped;

    public delegate void OnWormSectionsUpdatedDelegate(int totalWormSectionsAlive, int totalWormSectionsDead, int totalWormSections, int wormSectionsInHand, int sectionsPerBomb);
    public static OnWormSectionsUpdatedDelegate OnWormSectionsUpdated;      // for UI

    public delegate void OnTailSectionScatteredDelegate(Vector3 position, Bomb.BombColourType colourType);
    public static OnTailSectionScatteredDelegate OnTailSectionScattered;    // spawn a (silver) bomb!

    // Drag / Flick

    public delegate void OnFlickableActivatedDelegate(Flick flickObject);
    public static OnFlickableActivatedDelegate OnFlickableActivated;

    public delegate void OnFlickableDraggedDelegate(Flick flickObject);
    public static OnFlickableDraggedDelegate OnFlickableDragged;

    public delegate void OnFlickableThrownDelegate(Flick flickObject, Vector3 throwForce);
    public static OnFlickableThrownDelegate OnFlickableThrown;

    // Game status

    public delegate void OnGameStartedDelegate();
    public static OnGameStartedDelegate OnGameStarted;

    public delegate void OnGameOverDelegate(int score);
    public static OnGameOverDelegate OnGameOver;

    // Camera / Walls

    public delegate void OnWallsPositionedDelegate(float cameraHeight, float cameraWidth, Transform leftWall, Transform rightWall, Transform topWall, Transform bottomWall);
    public static OnWallsPositionedDelegate OnWallsPositioned;
}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public AudioSource gameMusic;           // plays on awake
    public AudioSource fartMusic;           // plays on awake

    private float fadeTime = 1f;
    private float musicFadeVolume = 0f; //0.05f;
    private float fartFadeVolume = 0.3f;


    private void OnEnable()
    {
        GameEvents.OnGameOver += OnGameOver;
    }

    private void OnDisable()
    {
        GameEvents.OnGameOver -= OnGameOver;
    }

    private void OnGameOver(int score)
    {
        FadeOutMusic();
    }

    // fade music on game over
    private void FadeOutMusic()
    {
        LeanTween.value(gameMusic.volume, musicFadeVolume, fadeTime)
                    .setOnUpdate((float f) => gameMusic.volume = f);

        LeanTween.value(fartMusic.volume, fartFadeVolume, fadeTime)
                    .setOnUpdate((float f) => fartMusic.volume = f);
    }
}

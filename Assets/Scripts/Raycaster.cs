using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Raycaster : MonoBehaviour
{
    [SerializeField]
    private LayerMask raycastTouchLayers;       // for 'activating' objects to drag / throw on touch 'on'

    [SerializeField]
    private LayerMask raycastDragLayers;        // for dragging object towards a point (eg. on ground, through water)

    [SerializeField]
    private LayerMask raycastThrowLayers;       // for throwing object towards a point (eg. on ground)

    [SerializeField]
    private LayerMask raycastBombLayers;       // for placing bombs

    private float raycastDistance = 1000f;
    private Flick activatedFlickable = null;          // 'flickable' object that raycast hit and activated

    private float bombCastRadius = 3f;          // sphere cast

    [SerializeField]
    private ParticleSystem throwParticles;

    //private bool throwOnRelease = true;         // otherwise drag to release point

    private Camera mainCam;



    private void OnEnable()
    {
        GameEvents.OnTouchOn += OnTouchOn;
        GameEvents.OnTouchMove += OnTouchMove;
        GameEvents.OnTouchOff += OnTouchOff;
    }

    private void OnDisable()
    {
        GameEvents.OnTouchOn -= OnTouchOn;
        GameEvents.OnTouchMove -= OnTouchMove;
        GameEvents.OnTouchOff -= OnTouchOff;
    }

    private void Start()
    {
        // get Camera.main once only for efficiency
        mainCam = Camera.main;
    }

    // event handlers for touch on/ move / off events

    private void OnTouchOn(Vector2 screenPosition, int touchCount)
    {
        ActivateRaycast(screenPosition);
    }

    private void OnTouchMove(Vector2 screenPosition, int touchCount, Vector2 direction, TimeSpan timeFromStart)
    {
        DragToRaycast(screenPosition);
    }

    private void OnTouchOff(Vector2 screenPosition, int touchCount, Vector2 direction, TimeSpan timeFromStart)
    {
        ThrowToRaycast(screenPosition);
    }

    // raycast for target collider layer from touch screenPosition and Flick component
    // activate the hit Flick (eg. animation)
    private void ActivateRaycast(Vector2 screenPosition)
    {
        // raycast to detect what was touched
        RaycastHit hitInfo = DoRaycast(screenPosition, raycastTouchLayers);

        if (hitInfo.transform != null)
        {
            //Debug.Log($"TouchRaycast HIT: {hitInfo.transform.name}");
            activatedFlickable = hitInfo.transform.GetComponent<Flick>();
            if (activatedFlickable != null)
            {
                activatedFlickable.Activate();
                return;
            }

            var wormSection = hitInfo.transform.GetComponent<WormSection3D>();
            if (wormSection != null)        // worm section without a Flick component - pop it!
            {
                wormSection.Pop();
                return;
            }
        }
        else
        {
            // raycast to place a bomb
            hitInfo = SetBombRaycast(screenPosition, raycastBombLayers);

            if (hitInfo.transform != null)
            {
                // fire bomb activated event
                GameEvents.OnBombActivated?.Invoke(hitInfo.point);
            }
        }
    }

    // raycast for throw 'destination' layer from touch screenPosition, drag hitFlick to hitPoint
    private void DragToRaycast(Vector2 screenPosition)
    {
        if (activatedFlickable == null)
            return;

        // raycast to detect what was touched
        RaycastHit hitInfo = DoRaycast(screenPosition, raycastDragLayers);

        if (hitInfo.transform != null)
        {
            //Debug.Log($"MoveRaycast HIT: {hitInfo.transform.name}");
            activatedFlickable.Drag(hitInfo.point);       // hitFlick maintains its original y pos
        }
    }

    // raycast for throw 'destination' layer from touch screenPosition, throw hitFlick in direction of hitPoint
    // on touch off event handler
    private void ThrowToRaycast(Vector2 screenPosition)
    {
        if (activatedFlickable == null)
            return;

        // raycast to detect what was touched
        RaycastHit hitInfo = DoRaycast(screenPosition, raycastThrowLayers);

        // throw in direction of hit point
        if (hitInfo.transform != null)
        {
            //Debug.Log($"ThrowRaycast HIT: {hitInfo.transform.name}");

            activatedFlickable.Throw(hitInfo.point);

            if (hitInfo.transform != activatedFlickable.transform)   // not still raycasting same flickable object
            {
                throwParticles.transform.position = hitInfo.point;
                throwParticles.Play();
            }
        }
        else        // catch-all throw to ensure unfreeze etc.
        {
            var throwPoint = mainCam.ScreenToWorldPoint(screenPosition);
            activatedFlickable.Throw(throwPoint);
            //GameEvents.OnFlickableThrown?.Invoke(activatedFlickable);
        }

        activatedFlickable = null;
    }

    private RaycastHit DoRaycast(Vector2 touchPosition, LayerMask layers)
    {
        Ray ray = mainCam.ScreenPointToRay(touchPosition);

        // raycast to detect what was touched
        RaycastHit hitInfo;
        Physics.Raycast(ray, out hitInfo, raycastDistance, layers);

        return hitInfo;
    }


    private RaycastHit SetBombRaycast(Vector2 touchPosition, LayerMask layers)
    {
        Ray ray = mainCam.ScreenPointToRay(touchPosition);

        // raycast to detect what was touched
        RaycastHit hitInfo;
        Physics.SphereCast(ray, bombCastRadius, out hitInfo, raycastDistance, layers);

        return hitInfo;
    }
}

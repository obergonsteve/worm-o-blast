using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using System;


public class CameraController : MonoBehaviour
{
    [SerializeField]
    private CinemachineVirtualCamera bombCamera;
    [SerializeField]
    private CinemachineVirtualCamera overheadBombCamera;
    [SerializeField]
    private CinemachineVirtualCamera roomCamera;

    private List<Bomb> bombQueue = new List<Bomb>();

    private float cameraShakeIntensity = 24f;
    private float cameraShakeTime = 0.5f;

    private void OnEnable()
    {
        GameEvents.OnBombSpawned += OnBombSpawned;
        GameEvents.OnBombDetonated += OnBombDetonated;
    }

    private void OnDisable()
    {
        GameEvents.OnBombSpawned -= OnBombSpawned;
        GameEvents.OnBombDetonated -= OnBombDetonated;
    }


    private void Start()
    {
        RoomCam();
    }

    private void OnBombSpawned(Bomb bomb)
    {
        bombQueue.Add(bomb);

        switch (bomb.BombColour)
        {
            case Bomb.BombColourType.Default:
                BombCam(bomb);
                break;

            default:
                RoomCam();
                //OverheadBombCam(bomb);
                break;

            case Bomb.BombColourType.Silver:
                OverheadBombCam(bomb);
                break;
        }
    }

    private void OnBombDetonated(Bomb bomb)
    {
        bombQueue.Remove(bomb);

        CameraShake(bombCamera, cameraShakeIntensity, cameraShakeTime);
        CameraShake(overheadBombCamera, cameraShakeIntensity, cameraShakeTime);
        CameraShake(roomCamera, cameraShakeIntensity, cameraShakeTime);

        //if (bombQueue.Count == 0)
            RoomCam();
        //else
        //    OverheadBombCam(bombQueue[0]);
    }

    //private void OnSilverBombSpawned(Bomb bomb)
    //{
    //    RoomCam();      // force camera switch?
    //    BombCam(bomb);
    //}

    private void RoomCam()
    {
        roomCamera.enabled = true;
        bombCamera.enabled = false;
        overheadBombCamera.enabled = false;

        bombCamera.LookAt = null;
        bombCamera.Follow = null;
    }

    private void BombCam(Bomb bomb)
    {
        bombCamera.LookAt = bomb.transform;
        bombCamera.Follow = bomb.transform;

        bombCamera.enabled = true;
        overheadBombCamera.enabled = false;
        roomCamera.enabled = false;
    }

    private void OverheadBombCam(Bomb bomb)
    {
        overheadBombCamera.LookAt = bomb.transform;
        overheadBombCamera.Follow = bomb.transform;

        bombCamera.enabled = false;
        overheadBombCamera.enabled = true;
        roomCamera.enabled = false;
    }

    private void CameraShake(CinemachineVirtualCamera camera, float intensity, float time)
    {
        CinemachineBasicMultiChannelPerlin noiseChanel = camera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();

        LeanTween.value(intensity, 0f, time)
            .setOnUpdate((float f) => noiseChanel.m_AmplitudeGain = f)
            .setEaseInCubic();
    }
}

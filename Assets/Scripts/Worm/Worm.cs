using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class Worm : MonoBehaviour
{
    [SerializeField]
    private LineRenderer lineRenderer;

    [SerializeField]
    private float shrinkDelay = 1f;
    [SerializeField]
    private float shrinkTime = 3f;

    [SerializeField]
    private List<WormSection> sections;

    [SerializeField]
    private float gravityScale = 1f;

    [SerializeField]
    private Vector2 startForce;

    private float unhingeInterval = 0.1f;
    private float unhingeTime = 0.5f;

    public bool InBucket { get; private set; } = false;
    public bool OnFloor { get; private set; } = false;
    //public bool HasStopped => InBucket || OnFloor;

    //public WormSection WormHead => sections[0];     // NOTE: assumes first section IsHead
    public WormSection WormHead => sections.FirstOrDefault(s => s.IsHead);

    private bool isFrozen = true;

    //public bool HasStopped
    //{
    //    get
    //    {
    //        foreach (var section in sections)
    //        {
    //            if (!section.HasStopped)
    //                return false;
    //        }

    //        return true;
    //    }
    //}


    private void Awake()
    {
        InitGravity();
        InitForce();
    }

    private void Start()
    {
        lineRenderer.positionCount = sections.Count;

        //pulsing = true;
        //StartCoroutine(PulseSections());
    }

    private void OnEnable()
    {
        WormEvents.OnFreezeWorms += OnFreezeWorms;

        WormEvents.OnWormEnterBucket += OnWormEnterBucket;
        WormEvents.OnWormExitBucket += OnWormExitBucket;
        WormEvents.OnWormHitFloor += OnWormHitFloor;
    }

    private void OnDisable()
    {
        WormEvents.OnFreezeWorms -= OnFreezeWorms;

        WormEvents.OnWormEnterBucket -= OnWormEnterBucket;
        WormEvents.OnWormExitBucket -= OnWormExitBucket;
        WormEvents.OnWormHitFloor -= OnWormHitFloor;
    }

    void Update()
    {
        DrawLine();
    }


    private void DrawLine()
    {
        // set renderer positions to positions of all of the points 
        lineRenderer.SetPositions(sections.Select(p => p.transform.position).ToArray());
    }


    private void OnFreezeWorms(bool freeze, int freezesLeft)
    {
        isFrozen = freeze;
        FreezeWorm(isFrozen);
    }


    public void FreezeWorm(bool isFrozen)
    {
        foreach (var section in sections)
        {
            var rigidBody = section.GetComponent<Rigidbody2D>();
            if (rigidBody != null)
            {
                rigidBody.simulated = !isFrozen;
            }
        }

        if (!isFrozen)
        {
            InitGravity();
            InitForce();
        }
    }

    private void OnWormEnterBucket(Worm worm, List<Worm> wormsInBucket)
    {
        if (this == worm)
            InBucket = true;
    }

    private void OnWormExitBucket(Worm worm, List<Worm> wormsInBucket)
    {
        if (this == worm)
            InBucket = false;
    }

    private void OnWormHitFloor(Worm worm, List<Worm> wormsOnFloor)
    {
        if (this != worm)
            return;

        if (OnFloor)
            return;

        OnFloor = true;
        Shrink();
    }

    private void Shrink()
    {
        //var startWidth = lineRenderer.startWidth;

        //LeanTween.value(startWidth, 0, shrinkTime)
        //            .setDelay(shrinkDelay)
        //            .setEaseInQuad()
        //            .setOnUpdate((float width) =>
        //            {
        //                lineRenderer.startWidth = width;
        //                lineRenderer.endWidth = width;
        //            })
        //            .setOnComplete(() => { Destroy(gameObject); });

        //// shrink section colliders
        //foreach (var section in sections)
        //{
        //    section.Shrink(shrinkDelay, shrinkTime);
        //}
    }

    private void InitGravity()
    {
        foreach (var section in sections)
        {
            var rigidBody = section.GetComponent<Rigidbody2D>();
            if (rigidBody != null)
            {
                rigidBody.gravityScale = this.gravityScale;
            }
        }
    }

    private void InitForce()
    {
        if (WormHead == null)
        {
            Debug.LogError("InitForce: null WormHead!");
            return;
        }

        WormHead.GetComponent<Rigidbody2D>().AddForce(startForce);
    }


    public IEnumerator UnHinge()
    {
        foreach (var section in sections)
        {
            var hinge = section.GetComponent<HingeJoint2D>();
            if (hinge != null)
            {
                hinge.enabled = false;
                yield return new WaitForSeconds(unhingeInterval);
            }
        }

        yield return new WaitForSeconds(unhingeTime);

        //StartCoroutine(Hinge());
        Hinge();
    }

    //public IEnumerator Hinge()
    public void Hinge()
    {
        foreach (var section in sections)
        {
            var hinge = section.GetComponent<HingeJoint2D>();
            if (hinge != null)
            {
                hinge.enabled = true;
                //yield return new WaitForSeconds(unhingeInterval);
            }
        }
    }

    //private IEnumerator PulseSections()
    //{
    //    while (pulsing)
    //    {
    //        foreach (var section in sections)
    //        {
    //            section.Pulse();
    //            yield return new WaitForSeconds(pulseInterval);
    //        }
    //    }

    //    yield return null;
    //}


    // while in editor only
    private void OnDrawGizmos()
    {
        lineRenderer.positionCount = sections.Count;
        DrawLine();
    }

}

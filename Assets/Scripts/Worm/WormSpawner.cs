using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


public class WormSpawner : MonoBehaviour
{
    public List<Worm3D> wormPrefabs = new List<Worm3D>();
    private List<Worm3D> activeWorms = new List<Worm3D>();

    private int aliveWorms => activeWorms.Count(w => !w.IsDead);

    private float spawnInterval = 5f;
    private float spawnX = 120f;        // set according to camera size
    private float spawnZ = 65f;         // set according to camera size

    private float cameraMargin = 10f;   // worms not spawned near to edges

    public Worm3D RandomPrefab => wormPrefabs[Random.Range(0, wormPrefabs.Count)];
    public Vector3 RandomSpawnPosition => new Vector3(Random.Range(-spawnX / 2f, spawnX / 2f), 0f, Random.Range(-spawnZ / 2f, spawnZ / 2f));

    private bool spawning = false;

    private Bomb.BombColourType nextWormTailType = Bomb.BombColourType.Default;    // type of tail to spawn next - overrides worm colour (eg. silver)

    private int silverSectionInitInterval = 50;     // initial number of sections spawned until silver worm tail spawned
    private int silverSectionMinInterval = 40;     // min number of sections spawned until silver worm tail spawned
    private int silverSectionMaxInterval = 60;     // max number of sections spawned until silver worm tail spawned

    // more alive worms means faster countdown
    private int silverSectionRandomInterval => Random.Range(silverSectionMinInterval, silverSectionMaxInterval) * aliveWorms;  
    private int silverSectionCountdown = 0;        // spawns remaining until silver worm tail
    private bool silverCountdownStarted = false;    // true when first worm hits ground

    private int wormsSpawned = 0;                   // number of worms spawned (not currently used)
    private int sectionsSpawned = 0;                // number of worm sections spawned (not currently used)
    private int silverSectionsSpawned = 0;          // number of silver sections spawned (not currently used)


    private void Start()
    {
        spawning = false;
        wormsSpawned = 0;
        sectionsSpawned = 0;
        silverSectionsSpawned = 0;

        StartCoroutine(SpawnWorms());
    }

    private void OnEnable()
    {
        GameEvents.OnWallsPositioned += OnWallsPositioned;
        GameEvents.OnWormHitGround += OnWormHitGround;              // first worm
        GameEvents.OnWormSectionSpawned += OnWormSectionSpawned;
        GameEvents.OnWormSectionPopped += OnWormSectionPopped;
        GameEvents.OnGameOver += OnGameOver;
    }

    private void OnDisable()
    {
        GameEvents.OnWallsPositioned -= OnWallsPositioned;
        GameEvents.OnWormHitGround -= OnWormHitGround;              // first worm
        GameEvents.OnWormSectionSpawned -= OnWormSectionSpawned;
        GameEvents.OnWormSectionPopped -= OnWormSectionPopped;
        GameEvents.OnGameOver -= OnGameOver;
    }


    private void OnWormHitGround(Worm3D worm)
    {
        silverCountdownStarted = true;
    }

    private void OnWallsPositioned(float cameraHeight, float cameraWidth, Transform leftWall, Transform rightWall, Transform topWall, Transform bottomWall)
    {
        spawnX = cameraWidth - cameraMargin;
        spawnZ = cameraHeight - cameraMargin;
    }

    private IEnumerator SpawnWorms()
    {
        if (spawning)
            yield break;

        spawning = true;
        silverSectionCountdown = silverSectionInitInterval;      // low number on game start
        GameEvents.OnSilverCountdownChanged?.Invoke(silverSectionCountdown);

        while (spawning)
        {
            SpawnWorm();
            yield return new WaitForSeconds(spawnInterval);
        }
    }

    private void SpawnWorm()
    {
        var prefab = RandomPrefab;

        var newWorm = Instantiate(prefab, transform);
        newWorm.transform.position = transform.position + RandomSpawnPosition;

        var tailColour = nextWormTailType == Bomb.BombColourType.Silver ? Bomb.BombColourType.Silver : prefab.tailColourType;

        newWorm.SpawnSections(tailColour);
        nextWormTailType = Bomb.BombColourType.Default;           // back to default worm colour

        activeWorms.Add(newWorm);
        wormsSpawned++;

        GameEvents.OnWormSpawned?.Invoke(newWorm);
    }

    private void OnWormSectionSpawned(WormSection3D worm)
    {
        if (worm.WormTailColourType == Bomb.BombColourType.Silver)
            silverSectionsSpawned++;

        sectionsSpawned++;

        if (silverCountdownStarted)
        {
            silverSectionCountdown--;

            if (silverSectionCountdown <= 0)        // next worm tail to be silver
            {
                nextWormTailType = Bomb.BombColourType.Silver;
                silverSectionCountdown = silverSectionRandomInterval;      // reset to new random number
            }

            GameEvents.OnSilverCountdownChanged?.Invoke(silverSectionCountdown);
        }
    }

    // destroy worm if no more sections (ie. all popped)
    private void OnWormSectionPopped(WormSection3D section, Vector3 position)
    {
        if (! section.ParentWorm.RemoveSection(section))        // this was last section, detroy worm!
        {
            activeWorms.Remove(section.ParentWorm);
            Destroy(section.ParentWorm);
        }
    }

    private void OnGameOver(int score)
    {
        spawning = false;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(transform.position, new Vector3(spawnX, 1f, spawnZ));
    }
}

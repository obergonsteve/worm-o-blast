using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(CircleCollider2D))]
[RequireComponent(typeof(Rigidbody2D))]

public class WormSection : MonoBehaviour
{
    private AudioSource audioSource;

    public bool IsHead;

    [SerializeField]
    private AudioClip hitHeadAudio;
    [SerializeField]
    private AudioClip hitAudio;         // not head section
    [SerializeField]
    private AudioClip hitFloorAudio;

    public Worm ParentWorm => transform.parent.GetComponent<Worm>();
    public bool IsCentre => GetComponent<WormCentre>() != null;

    private bool shrinking = false;

    private CircleCollider2D sectionCollider;       // for shrinking - so stays on floor
    private Rigidbody2D sectionRigidbody;           // to detect when stopped


    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        sectionCollider = GetComponent<CircleCollider2D>();
        sectionRigidbody = GetComponent<Rigidbody2D>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (ParentWorm.InBucket)
            return;

        if (shrinking)
            return;

        if (collision.gameObject.CompareTag("Floor"))
        {
            if (hitFloorAudio != null)  
            {
                audioSource.clip = hitFloorAudio;
                audioSource.Play();
            }
        }
        else if (IsHead)
        {
            //if (collision.gameObject.CompareTag("Obstacle"))
            //{
            //    //Destroy(collision.gameObject);
            //}

            if (hitHeadAudio != null)
            {
                audioSource.clip = hitHeadAudio;
                audioSource.Play();
            }
        }
        else        // body / tail sections
        {
            if (hitAudio != null)     
            {
                audioSource.clip = hitAudio;
                audioSource.Play();
            }
        }

        if (IsCentre && collision.gameObject.CompareTag("Obstacle"))
            WormEvents.OnWormCentreHitObstacle?.Invoke(ParentWorm);
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (shrinking)
            return;

        if (IsCentre && collision.gameObject.CompareTag("Obstacle"))
            WormEvents.OnWormCentreLeftObstacle?.Invoke(ParentWorm);
    }

    public void Shrink(float shrinkDelay, float shrinkTime)
    {
        //if (shrinking)
        //    return;

        //shrinking = true;
        //var startWidth = sectionCollider.radius;

        //LeanTween.value(startWidth, 0, shrinkTime)
        //            .setDelay(shrinkDelay)
        //            .setEaseInCubic()
        //            .setOnUpdate((float width) =>
        //            {
        //                sectionCollider.radius = width;
        //            });
    }

    //public bool HasStopped => sectionRigidbody.velocity.magnitude < 0.001;
    //public bool IsSleeping => sectionRigidbody.IsSleeping();
}

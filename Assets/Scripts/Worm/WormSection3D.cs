using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(SphereCollider))]
[RequireComponent(typeof(Rigidbody))]

public class WormSection3D : MonoBehaviour
{
    public bool IsTail;

    private float initialHealth = 200f;

    [SerializeField]
    public float CurrentHealth { get; private set; } = 0f;

    private float HealthPercent => CurrentHealth / initialHealth;
    public bool IsDead => (CurrentHealth <= 0f);

    [SerializeField]
    private Material healthyMaterial;
    [SerializeField]
    private Material damagedMaterial;
    [SerializeField]
    private Material deadMaterial;
    [SerializeField]
    private Material deadWormMaterial;      // parent worm (all sections) dead
    [SerializeField]
    private Material frozenMaterial;
    [SerializeField]
    private Material scatteredMaterial;
    [SerializeField]
    private Material silverMaterial;

    private Vector3 startScale = Vector3.one;          // set in PulseIn
    [SerializeField]
    private float pulseScale = 1.25f;       // factor

    private int scaleInTweenId = 0;

    [SerializeField]
    private AudioClip hitHeadAudio;
    [SerializeField]
    private AudioClip hitAudio;         // not head section
    [SerializeField]
    private AudioClip hitFloorAudio;

    [SerializeField]
    private AudioClip popAudio;
    private float popMinPitch = 2.5f;
    private float popMaxPitch = 3f;
    private float popPitch => UnityEngine.Random.Range(popMinPitch, popMaxPitch);

    [SerializeField]
    private GameObject thrownParticlesPrefab;
    private ParticleSystem thrownParticles;     // instance

    private float throwForceFactor = 5f;        // throw force factor (if physics) - per worm section
    private float maxThrowForceFactor = 80f;     // throw force factor (if physics)

    public Worm3D ParentWorm => transform.parent.GetComponent<Worm3D>();

    private bool shrinking = false;
    //private float scatterShrinkDelay = 30f;
    //private float shrinkTime = 120f;

    private bool pulsing = false;

    private float popMinDelay = 20f;        // seconds
    private float popMaxDelay = 60f;        // seconds
    private float popDelay => UnityEngine.Random.Range(popMinDelay, popMaxDelay);
    private float popDestroyDelay = 10f;    // destroyed after popped

    private Vector3 freezeFrameVelocity; 
    private Vector3 freezeFrameAngular; 

    public bool IsFrozen { get; private set; }
    public bool IsScattered { get; private set; }

    public Bomb.BombColourType WormTailColourType => ParentWorm.tailColourType;

    private SphereCollider sectionCollider;       // for shrinking - so stays on floor
    private Rigidbody sectionRigidbody;           // to detect when stopped
    private Renderer sectionRenderer;        
    private HingeJoint sectionHinge;

    private AudioSource audioSource;
    private float initialMass;


    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        sectionCollider = GetComponent<SphereCollider>();
        sectionRigidbody = GetComponent<Rigidbody>();
        sectionHinge = GetComponent<HingeJoint>();
        sectionRenderer = GetComponent<Renderer>();

        initialMass = sectionRigidbody.mass;

        CurrentHealth = initialHealth;

        if (thrownParticlesPrefab != null)
            thrownParticles = Instantiate(thrownParticlesPrefab, transform).GetComponent<ParticleSystem>();
    }

    private void OnEnable()
    {
        GameEvents.OnFlickableActivated += OnFlickableActivated;
        //GameEvents.OnFlickableDragged += OnFlickableDragged;
        GameEvents.OnFlickableThrown += OnFlickableThrown;
    }

    private void OnDisable()
    {
        GameEvents.OnFlickableActivated -= OnFlickableActivated;
        //GameEvents.OnFlickableDragged -= OnFlickableDragged;
        GameEvents.OnFlickableThrown -= OnFlickableThrown;
    }


    private void OnCollisionEnter(Collision collision)
    {
        if (shrinking)
            return;

        if (collision.gameObject.CompareTag("Ground"))
        {
            if (hitFloorAudio != null)
            {
                audioSource.clip = hitFloorAudio;
                audioSource.Play();
            }

            ParentWorm.HitGround();
        }
        //else if (IsHead)
        //{
        //    if (hitHeadAudio != null)
        //    {
        //        audioSource.clip = hitHeadAudio;
        //        audioSource.Play();
        //    }
        //}
        else        // body / tail sections
        {
            if (hitAudio != null)
            {
                audioSource.clip = hitAudio;
                audioSource.Play();
            }
        }
    }


    public void TakeDamage(float damage, int fromBombId)
    {
        if (ParentWorm.IsDead)      // already dead (by a previous bomb)
        {
            //Debug.Log($"TakeDamage: fromBombId = {fromBombId}");
            // scatter worm if not same bomb that killed it
            ParentWorm.Scatter(fromBombId);            // unhinge all sections, collect section count to make new bombs
            return;
        }

        if (IsDead || IsScattered)     // already dead
            return;

        CurrentHealth -= damage;
        if (CurrentHealth < 0)
            CurrentHealth = 0;

        if (IsDead)     // became dead from this damage
        {
            ParentWorm.CheckIfDead(fromBombId);       // freeze if all sections now dead
        }

        SetMaterial();

        // mass increases as health decreases
        //sectionRigidbody.mass = (HealthPercent > 0f ? (initialMass / HealthPercent) : 0f);

        // mass decreases as health decreases
        //sectionRigidbody.mass = initialMass * HealthPercent;
    }

    // all sections of a worm scattered together
    public void Scatter(float upForce, float mass)
    {
        if (IsScattered)
            return;

        IsScattered = true;
        UnFreeze();

        if (sectionHinge != null)
            Destroy(sectionHinge);     // remove hinge component

        if (sectionRigidbody != null)
        {
            sectionRigidbody.useGravity = false;
            sectionRigidbody.isKinematic = false;
            sectionRigidbody.mass = mass;

            sectionRigidbody.AddForce(Vector3.up * upForce, ForceMode.Impulse);
        }

        SetMaterial();

        //Shrink(scatterShrinkDelay, shrinkTime);
        StartCoroutine(Pop(popDelay));

        if (IsTail)
        {
            GameEvents.OnTailSectionScattered?.Invoke(transform.position, WormTailColourType);
        }
    }

    public void InitTail()
    {
        IsTail = true;
        SetMaterial();
    }


    private void SetMaterial()
    {
        if (IsScattered)
        {
            sectionRenderer.material = scatteredMaterial;
        }
        else if (IsTail && WormTailColourType == Bomb.BombColourType.Silver)
        {
            sectionRenderer.material = silverMaterial;
        }
        else if (ParentWorm.IsDead)    
        {
            sectionRenderer.material = deadWormMaterial;
        }
        else if (IsFrozen)
        {
            sectionRenderer.material = frozenMaterial;
        }
        else if (IsDead)
        {
            sectionRenderer.material = deadMaterial;
        }
        else if (HealthPercent < 0.5f)
        {
            sectionRenderer.material = damagedMaterial;
        }
        else
        {
            sectionRenderer.material = healthyMaterial;
        }
    }

    public void Pop()
    {
        StartCoroutine(Pop(0f));
    }

    private IEnumerator Pop(float delay)
    {
        yield return new WaitForSeconds(delay);
        GameEvents.OnWormSectionPopped?.Invoke(this, transform.position);     // eg. pop particles

        sectionCollider.enabled = false;
        sectionRenderer.enabled = false;

        if (popAudio != null)
        {
            audioSource.clip = popAudio;
            audioSource.pitch = popPitch;
            audioSource.Play();
        }

        Destroy(gameObject, popDestroyDelay);
    }

    //private void Shrink(float shrinkDelay, float shrinkTime)
    //{
    //    if (shrinking)
    //        return;

    //    shrinking = true;
    //    var startWidth = sectionCollider.radius;

    //    LeanTween.value(startWidth, 0, shrinkTime)
    //                .setDelay(shrinkDelay)
    //                .setEaseInCubic()
    //                .setOnUpdate((float width) =>
    //                {
    //                    sectionCollider.radius = width;
    //                });
    //}

    private void OnFlickableActivated(Flick flickObject)
    {
        if (flickObject.gameObject != gameObject)       // not this worm!
            return;

        if (ParentWorm == null)
            return;

        if (ParentWorm.IsDead)
            return;

        ParentWorm.Freeze();    // freeze all sections
    }

    //private void OnFlickableDragged(Flick flickObject)
    //{
        //if (flickObject.gameObject != gameObject)       // not this worm!
        //    return;

        //if (ParentWorm == null)
        //    return;

        //if (ParentWorm.IsDead)
        //    return;

        //ParentWorm.UnFreeze();    // unfreeze all sections
    //}

    private void OnFlickableThrown(Flick flickObject, Vector3 throwForce)
    {
        if (flickObject.gameObject != gameObject)       // not this worm section thrown!
            return;

        if (ParentWorm == null)
            return;

        if (ParentWorm.IsDead)
            return;

        ParentWorm.UnFreeze();    // unfreeze all sections

        var clampedForce = Mathf.Clamp(throwForceFactor, throwForceFactor, maxThrowForceFactor);
        sectionRigidbody.AddForce(ParentWorm.SectionCount * clampedForce * throwForce, ForceMode.Impulse);

        if (thrownParticles != null)
            thrownParticles.Play();
    }

    public void Pulse(float pulseTime)
    {
        if (pulsing)
            return;

        pulsing = true;

        LeanTween.scale(gameObject, startScale * pulseScale, pulseTime / 2f)
                    .setEaseOutBack()
                    .setOnComplete(() =>
                    {
                        LeanTween.scale(gameObject, startScale, pulseTime / 2f)
                                    .setEaseInBack()
                                    .setOnComplete(() =>
                                    {
                                        pulsing = false;
                                    });
                    });
    }

    public void ScaleIn(float scaleInTime, Action onScaleIn = null)
    {
        startScale = transform.localScale;
        transform.localScale = Vector3.zero;

        scaleInTweenId = LeanTween.scale(gameObject, startScale, scaleInTime)
                            .setEaseOutElastic()
                            .setOnComplete(() =>
                            {
                                onScaleIn?.Invoke();
                                scaleInTweenId = 0;

                                // NOTE: snapshot mass here - assumes it's set correctly by now!  
                                initialMass = sectionRigidbody.mass;
                            }).id;
    }

    // all sections of a worm frozen together
    public void Freeze()
    {
        freezeFrameVelocity = sectionRigidbody.velocity;
        freezeFrameAngular = sectionRigidbody.angularVelocity;

        if (scaleInTweenId > 0 && LeanTween.isTweening(scaleInTweenId))
            LeanTween.pause(scaleInTweenId);

        Stop();
        IsFrozen = true;
        ActivatePhysics(false);

        SetMaterial();

        //if (IsDead)
        //{
        //    // remove flick component once dead
        //    var flick = GetComponent<Flick>();
        //    if (flick != null)
        //        Destroy(flick);
        //}
    }

    // all sections of a worm unfrozen together
    public void UnFreeze()
    {
        ActivatePhysics(true);

        sectionRigidbody.velocity = freezeFrameVelocity;
        sectionRigidbody.angularVelocity = freezeFrameAngular;
        IsFrozen = false;

        SetMaterial();

        if (LeanTween.isPaused(scaleInTweenId))
            LeanTween.resume(scaleInTweenId);
    }

    public void RemoveFlick()
    {
        // remove flick component once dead
        var flick = GetComponent<Flick>();
        if (flick != null)
            Destroy(flick);
    }

    private void ActivatePhysics(bool activate)
    {
        sectionRigidbody.isKinematic = !activate;

        if (IsFrozen && activate)
            IsFrozen = false;
    }

    public void Stop()
    {
        sectionRigidbody.velocity = Vector3.zero;
        sectionRigidbody.angularVelocity = Vector3.zero;
    }
}

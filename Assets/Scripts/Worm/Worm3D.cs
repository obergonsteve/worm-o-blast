using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

[RequireComponent(typeof(AudioSource))]

public class Worm3D : MonoBehaviour
{
    [SerializeField]
    private LineRenderer lineRenderer;

    [SerializeField]
    private float shrinkDelay = 1f;
    [SerializeField]
    private float shrinkTime = 3f;

    [SerializeField]
    private List<WormSection3D> sections;
    public int SectionCount => sections.Count;

    [SerializeField]
    private WormSection3D sectionPrefab;
    [SerializeField]
    private TailFace tailFacePrefab;
    private TailFace tailFace;        // instance

    private float pulseTime = 0.05f;     // each section

    private float spawnInterval = 1.5f;
    private float tailSpawnDelay = 0.25f;    // between head and tail
    private bool spawning = false;
    private Coroutine spawnCoroutine;

    public bool SpawnOnHitGround = true;        // only start spawning body sections once first section hits the ground
    private bool hitGround;                     // first section has hit the ground!
    public AudioClip hitGroundAudio;

    private float bodyScale = 1f;
    private float tailScale = 1.1f;               // should be slightly larger than body
    private float scaleInFactor = 0.25f;        // factor of spawnInterval (time to scale in new section)

    private int maxSections = 50;

    private float sectionSpeed = 1f;
    private float initialBoost = 10f;
    private float headMass = 8f;          // head section mass
    private float sectionMass = 4f;       // body sections
    private float tailMass = 8f;          // tail section

    private float tailFaceRotationSpeed = 5f;
    private float minTailSpeed = 1f;

    private WormSection3D tailSection;

    public Bomb.BombColourType tailColourType = Bomb.BombColourType.Default;        // type of tail to spawn
    public Material HealthySectionMaterial;            // to set eye colour

    private float scatteredUpForce = 10f;         // scattered worms' detached sections pushed upwards
    private float scatteredMass = 2f;          // scattered worms' detached sections pushed upwards

    //[SerializeField]
    //private StatsUI statsPrefab;          // attached to tail

    public WormSection3D FirstSection => sections.Count == 0 ? null : sections[0];
    public WormSection3D LastSection => sections[ sections.Count-1 ];
    public WormSection3D LastHingeSection => sections[ sections.Count-2 ];      // next to tail

    public bool IsFrozen { get; private set; } = false;
    public bool IsDead { get; private set; } = false;
    public bool IsScattered { get; private set; } = false;

    private float deadFreezeDelay = 0.25f;
    private int killedByBomb = -1;       // bomb that 'killed' this worm

    public AudioClip headSpawnedAudio;
    public AudioClip deadAudio;
    public AudioClip scatteredAudio;

    private AudioSource audioSource;


    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void OnEnable()
    {
        GameEvents.OnGameOver += OnGameOver;
    }

    private void OnDisable()
    {
        GameEvents.OnGameOver -= OnGameOver;
    }

    private void FixedUpdate()
    {
        if (!IsDead)
            DrawLine();

        if (!IsFrozen)       // while dragging
        {
            if (!SpawnOnHitGround || hitGround)     // use initial down force until hit ground
            {
                // tail pulls whole worm
                SetTailSpeed(false);        // maintain constant (normalised) velocity
            }
        }
    }


    public void SpawnSections(Bomb.BombColourType tailType)
    {
        tailColourType = tailType;
        spawnCoroutine = StartCoroutine(SpawnWormSections());
    }


    private IEnumerator SpawnWormSections()
    {
        if (spawning)
            yield break;

        spawning = true;
        sections.Clear();

        while (spawning)
        {
            while (IsFrozen)
            {
                yield return null;
            }

            if (IsDead)
            {
                spawning = false;
                yield break;
            }

            if (SectionCount == 0)    // spawn the head section
            {
                SpawnHead();
            }
            else if (SectionCount == 1)       // currently head only, spawn the tail section
            {
                SpawnTail();
            }
            else    // spawn a new body section, just before tail
            {
                while (SpawnOnHitGround && !hitGround)      // wait until hit ground
                {
                    yield return null;
                }

                SpawnBody();
            }

            // pulse all sections (except tail)
            StartCoroutine(PulseAllSections());

            if (SectionCount == 1)      // no pause between head and tail
                yield return new WaitForSeconds(tailSpawnDelay);
            else
                yield return new WaitForSeconds(spawnInterval);
        }
    }

    private void StopSpawning()
    {
        if (spawnCoroutine != null)
        {
            StopCoroutine(spawnCoroutine);
            spawnCoroutine = null;
        }

        spawning = false;
    }

    // first section to be spawned
    private void SpawnHead()
    {
        var newHeadSection = Instantiate(sectionPrefab, transform);
        sections.Add(newHeadSection);

        newHeadSection.name = "Head";
        //newHeadSection.IsHead = true;
        newHeadSection.transform.localScale = Vector3.one * bodyScale;

        Rigidbody newHeadRb = newHeadSection.GetComponent<Rigidbody>();

        newHeadRb.mass = headMass;
        newHeadSection.ScaleIn(spawnInterval * scaleInFactor); //, UnFreeze);

        audioSource.clip = headSpawnedAudio;
        audioSource.Play();

        GameEvents.OnWormSectionSpawned?.Invoke(newHeadSection);
    }

    private void SpawnTail()
    {
        if (FirstSection == null)
            return;

        HingeJoint headHinge = FirstSection.GetComponent<HingeJoint>();

        tailSection = Instantiate(sectionPrefab, transform);
        sections.Add(tailSection);

        tailFace = Instantiate(tailFacePrefab, tailSection.transform);

        tailSection.name = "Tail";
        tailSection.InitTail();     // set material, in case silver, etc.

        tailSection.transform.position = headHinge.transform.position;
        tailSection.transform.rotation = FirstSection.transform.rotation;
        tailSection.transform.localScale = Vector3.one * tailScale;

        Destroy(tailSection.GetComponent<HingeJoint>());      // tail has no hinge

        Rigidbody newTailRb = tailSection.GetComponent<Rigidbody>();
        newTailRb.mass = tailMass;

        tailSection.ScaleIn(spawnInterval * scaleInFactor); //, UnFreeze);

        // tail has constant velocity, to pull all other sections
        SetTailSpeed(true);

        headHinge.connectedBody = newTailRb;

        GameEvents.OnWormSectionSpawned?.Invoke(tailSection);

        // stats UI attached to tail
        //if (statsPrefab != null)
        //{
        //    var statsUI = Instantiate(statsPrefab, newTailSection.transform);
        //}
    }

    private void SpawnBody()
    {
        if (IsDead)
            return;

        var currentTail = LastSection;
        HingeJoint currentLastHinge = LastHingeSection.GetComponent<HingeJoint>();

        WormSection3D newBodySection = Instantiate(sectionPrefab, transform);
        sections.Insert(sections.Count - 1, newBodySection);        // before currentTail

        newBodySection.name = "Body" + (sections.Count - 2);      // exclude head and tail in count
        newBodySection.IsTail = false;
        newBodySection.transform.position = currentTail.transform.position;
        newBodySection.transform.rotation = currentLastHinge.transform.rotation;
        newBodySection.transform.localScale = Vector3.one * bodyScale;

        newBodySection.GetComponent<Rigidbody>().mass = sectionMass;

        currentLastHinge.connectedBody = null;

        // scale in currentTail so it looks like it's new
        // newBodySection will appear in place of currentTail
        currentTail.ScaleIn(spawnInterval * scaleInFactor);

        newBodySection.GetComponent<HingeJoint>().connectedBody = currentTail.GetComponent<Rigidbody>();
        currentLastHinge.connectedBody = newBodySection.GetComponent<Rigidbody>();

        // remove head if more than maxSections
        if (sections.Count > maxSections)
            RemoveHeadSection();    // no net gain in worm sections (1 added, 1 removed)
        else
            GameEvents.OnWormSectionSpawned?.Invoke(newBodySection);
    }

    // remove current head (first) section
    // and make new first section head
    private void RemoveHeadSection()
    {
        var firstSection = FirstSection;
        //GameEvents.OnWormHeadRemoved?.Invoke(firstSection);

        sections.RemoveAt(0);
        Destroy(firstSection.gameObject);

        // new first section!
        FirstSection.name = "Head";
        //FirstSection.IsHead = true;
    }

    // returns false if no more sections (eg. can destroy worm)
    public bool RemoveSection(WormSection3D section)
    {
        sections.Remove(section);
        return SectionCount > 0;
    }


    // maintain constant (normalised) velocity
    // tail pulls whole worm
    private void SetTailSpeed(bool init)
    {
        if (LastSection == null)
            return;

        var tailRigidbody = LastSection.GetComponent<Rigidbody>();
        var massRatio = sectionMass / headMass;

        if (init)   // initial velocity - downwards!
            tailRigidbody.velocity = sectionSpeed * initialBoost * Vector3.down;
        else        // maintain constant speed, according to number of sections
            tailRigidbody.velocity = sectionSpeed * (sections.Count * massRatio) * tailRigidbody.velocity.normalized;

        var movementDirection = tailRigidbody.velocity.normalized;
        if (movementDirection.magnitude > minTailSpeed)
            RotateTailFace(movementDirection);
    }

    // rotate tail face towards direction of movement
    private void RotateTailFace(Vector3 movementDirection)
    {
        //if (movementDirection.magnitude < minTailSpeed)
        //    return;

        if (tailFace == null)       // may have been destroyed
            return;

        Quaternion toRotation = Quaternion.LookRotation(movementDirection, Vector3.up);
        tailFace.transform.rotation = Quaternion.RotateTowards(tailFace.transform.rotation, toRotation, tailFaceRotationSpeed * Time.fixedDeltaTime);
    }

    //private void Stop()
    //{
    //    foreach (var section in sections)
    //    {
    //        section.Stop();
    //    }
    //}

    private IEnumerator DeadFreeze()
    {
        lineRenderer.enabled = false;
        yield return new WaitForSeconds(deadFreezeDelay);       // eg. to allow bomb force to affect

        if (! IsScattered)
            Freeze();

        audioSource.clip = deadAudio;
        audioSource.Play();
    }

    public void Freeze()
    {
        IsFrozen = true;

        foreach (var section in sections)
        {
            section.Freeze();
        }

        if (IsDead)
            lineRenderer.enabled = false;
    }

    public void UnFreeze()
    {
        foreach (var section in sections)
        {
            section.UnFreeze();
        }

        IsFrozen = false;

        if (!IsDead)
            lineRenderer.enabled = true;
    }

    // after a worm section takes damage
    public void CheckIfDead(int bombId)
    {
        if (IsDead)
            return;

        foreach (var section in sections)
        {
            if (! section.IsDead)
            {
                IsDead = false;
                return;
            }
        }

        IsDead = true;
        killedByBomb = bombId;

        StopSpawning();
        StartCoroutine(DeadFreeze());       // stop / kinematic

        RemoveFlick();          // remove Flick component from sections so can no longer be 'activated' (for freeze/drag/throw)

        if (tailFace != null && tailColourType != Bomb.BombColourType.Silver)
            tailFace.SetEyeColour(HealthySectionMaterial);

        GameEvents.OnWormDead?.Invoke(this);
    }

    // remove Flick component from sections so can no longer be 'activated' (for freeze/drag/throw)
    private void RemoveFlick()
    {
        // unhinge sections
        foreach (var section in sections)
        {
            section.RemoveFlick();
        }
    }

    public void Scatter(int fromBombId)
    {
        if (!IsDead)
            return;

        if (killedByBomb == fromBombId)      // must be scattered by a different bomb
            return;

        if (IsScattered)
            return;

        audioSource.clip = scatteredAudio;
        audioSource.Play();

        IsScattered = true;     // prevents multiple calls on multiple worm section triggered by bomb force
        lineRenderer.enabled = false;

        // unhinge sections
        foreach (var section in sections)
        {
            section.Scatter(scatteredUpForce, scatteredMass);
        }

        if (tailFace != null)       // may have been destroyed
            tailFace.gameObject.SetActive(false);

        GameEvents.OnWormScattered?.Invoke(this);
    }

    //public void ActivatePhysics(bool activate)
    //{
    //    foreach (var section in sections)
    //    {
    //        section.ActivatePhysics(activate);
    //    }

    //    if (IsFrozen && activate)
    //        IsFrozen = false;
    //}


    // pulse all but tail section, towards head
    private IEnumerator PulseAllSections()
    {
        for (int i = sections.Count-2; i >= 0; i--)
        {
            if (IsDead)
                yield break;

            while (IsFrozen)
            {
                yield return null;
            }

            var section = sections[i];
            section.Pulse(pulseTime);
            yield return new WaitForSeconds(pulseTime / 2f);
        }
    }

    private void OnGameOver(int score)
    {
        StopSpawning();
    }

    public void HitGround()
    {
        if (!SpawnOnHitGround || hitGround)
            return;

        hitGround = true;
        audioSource.clip = hitGroundAudio;
        audioSource.Play();

        GameEvents.OnWormHitGround?.Invoke(this);
    }

    private void DrawLine()
    {
        if (IsDead || IsScattered)
            return;

        lineRenderer.positionCount = sections.Count;

        // set renderer positions to positions of all of the sections 
        lineRenderer.SetPositions(sections.Select(p => p.transform.position).ToArray());
    }

    // while in editor only
    private void OnDrawGizmos()
    {
        if (! IsDead)
        {
            DrawLine();
        }
    }
}

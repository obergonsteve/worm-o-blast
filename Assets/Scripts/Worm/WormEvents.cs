using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class WormEvents
{
    // Worms

    public delegate void OnWormEnterBucketDelegate(Worm worm, List<Worm> wormsInBucket);
    public static OnWormEnterBucketDelegate OnWormEnterBucket;

    public delegate void OnWormExitBucketDelegate(Worm worm, List<Worm> wormsInBucket);
    public static OnWormExitBucketDelegate OnWormExitBucket;

    public delegate void OnWormCentreHitObstacleDelegate(Worm worm);
    public static OnWormCentreHitObstacleDelegate OnWormCentreHitObstacle;

    public delegate void OnWormCentreLeftObstacleDelegate(Worm worm);
    public static OnWormCentreLeftObstacleDelegate OnWormCentreLeftObstacle;

    public delegate void OnWormHitFloorDelegate(Worm worm, List<Worm> wormsOnFloor);
    public static OnWormHitFloorDelegate OnWormHitFloor;

    public delegate void OnFreezeWormsDelegate(bool frozen, int freezesLeft);
    public static OnFreezeWormsDelegate OnFreezeWorms;

    // Touch Input

    public delegate void OnTouchOnDelegate(Vector2 screenPosition, int touchCount);
    public static OnTouchOnDelegate OnTouchOn;

    public delegate void OnTouchMoveDelegate(Vector2 screenPosition, int touchCount, Vector2 startScreenPosition, Vector2 lastTouchPosition, TimeSpan timeFromStart);
    public static OnTouchMoveDelegate OnTouchMove;

    public delegate void OnTouchOffDelegate(Vector2 screenPosition, int touchCount, Vector2 startScreenPosition, TimeSpan timeFromStart);
    public static OnTouchOffDelegate OnTouchOff;

    // Bucket

    public delegate void OnBucketMoveDelegate(Vector2 screenPosition, Vector2 lastTouchPosition);
    public static OnBucketMoveDelegate OnBucketMove;

    public delegate void OnObstaclesMoveDelegate(Vector2 screenPosition, Vector2 lastTouchPosition);
    public static OnObstaclesMoveDelegate OnObstaclesMove;

    // Game

    public delegate void OnGameStatusChangedDelegate(GameManager.GameStatus newStatus);
    public static OnGameStatusChangedDelegate OnGameStatusChanged;

    public delegate void OnGameCountDownDelegate(int secondsRemaining);
    public static OnGameCountDownDelegate OnGameCountDown;
}

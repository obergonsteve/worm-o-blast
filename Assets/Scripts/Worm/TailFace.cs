using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TailFace : MonoBehaviour
{
    public MeshRenderer LeftEye;
    public MeshRenderer RightEye;


    public void SetEyeColour(Material eyeMaterial)
    {
        if (eyeMaterial == null)
            return;

        LeftEye.material = eyeMaterial;
        RightEye.material = eyeMaterial;
    }
}

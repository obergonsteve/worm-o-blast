using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Environment : MonoBehaviour
{
    public Transform LeftWall;
    public Transform RightWall;
    public Transform TopWall;
    public Transform BottomWall;

    private float wallThickness = 10f;
    private float wallHeight = 50f;
    private float wallVisible = 1.5f;

    private float DistanceFromCamera => mainCam.transform.position.y - transform.position.y;

    private float CameraHeight => 2.0f * DistanceFromCamera * Mathf.Tan(mainCam.fieldOfView * 0.5f * Mathf.Deg2Rad);
    private float CameraWidth => CameraHeight * mainCam.aspect;

    private float CameraTopZ => (CameraHeight / 2f) + mainCam.transform.position.z;
    private float CameraBottomZ => -(CameraHeight / 2f) + mainCam.transform.position.z;

    private float CameraRightX => (CameraWidth / 2f) + mainCam.transform.position.x;
    private float CameraLeftX => -(CameraWidth / 2f) + mainCam.transform.position.x;

    private Camera mainCam;


    private void Start()
    {
        mainCam = Camera.main;
        FitWallsToCamera();
    }

    private void FitWallsToCamera() 
    {
        var halfWall = wallThickness / 2f;
        var wallY = wallHeight / 2f;

        TopWall.localScale = new Vector3(CameraWidth + wallThickness, wallHeight, wallThickness);
        TopWall.localPosition = new Vector3(mainCam.transform.position.x, wallY, CameraTopZ + halfWall - wallVisible);

        BottomWall.localScale = new Vector3(CameraWidth + wallThickness, wallHeight, wallThickness);
        BottomWall.localPosition = new Vector3(mainCam.transform.position.x, wallY, CameraBottomZ - halfWall + wallVisible);

        LeftWall.localScale = new Vector3(wallThickness, wallHeight, CameraHeight + wallThickness);
        LeftWall.localPosition = new Vector3(CameraLeftX - halfWall + wallVisible, wallY, mainCam.transform.position.z);

        RightWall.localScale = new Vector3(wallThickness, wallHeight, CameraHeight + wallThickness);
        RightWall.localPosition = new Vector3(CameraRightX + halfWall - wallVisible, wallY, mainCam.transform.position.z);

        GameEvents.OnWallsPositioned?.Invoke(CameraHeight, CameraWidth, LeftWall, RightWall, TopWall, BottomWall);
    }
}

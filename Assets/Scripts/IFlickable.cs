using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public interface IFlickable
{
    public void Activate();

    public void Drag(Vector3 toPosition);

    public void Throw(Vector3 toPosition);
}

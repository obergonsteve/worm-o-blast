using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[RequireComponent(typeof(SphereCollider))]      // explosion force

public class BombForce : MonoBehaviour
{
    public float explodeStartRadius = 1f;        // trigger collider radius
    public float explodeEndRadius = 16f;            // trigger collider radius
    private float explodeTime = 0.2f;

    public float maxDamage = 360f;             // damage at time of explosion -> zero at end
    private float currentDamage = 0f;          // over explodeTime - does damage to worm sections
    public float forceFactor = 4f;            // for AddForce()

    private Bomb.BombColourType bombColourType;     // for explosion

    private SphereCollider forceTrigger;


    private void Awake()
    {
        forceTrigger = GetComponent<SphereCollider>();
    }

    public void Explode(Action onComplete)
    {
        LeanTween.value(explodeStartRadius, explodeEndRadius, explodeTime)
                        .setOnUpdate((float f) => { forceTrigger.radius = f; })
                        .setEaseOutSine()
                        .setOnComplete(() =>
                        {
                            forceTrigger.enabled = false;
                        });

        LeanTween.value(maxDamage, 0f, explodeTime)
                        .setOnUpdate((float f) => { currentDamage = f; })
                        .setEaseOutSine()
                        .setOnComplete(() =>
                        {
                            onComplete?.Invoke();
                        });
    }

    // push away rigidbodies
    private void OnTriggerStay(Collider other)
    {
        if (currentDamage == 0)
            return;

        if (other.gameObject.CompareTag("Bomb"))        // don't bomb bombs!
            return;

        var otherRb = other.attachedRigidbody;
        if (otherRb != null)
        {
            var forceDirection = (other.transform.position - transform.position).normalized;
            otherRb.AddForce(forceDirection * currentDamage * forceFactor, ForceMode.Impulse);
        }

        // black and silver bombs do damage to worm sections of all colours
        // bombs of all other colours do damage to worm sections of their own colour only ... and silver
        var wormSection = other.GetComponent<WormSection3D>();
        if (wormSection != null)
        {
            if (bombColourType == Bomb.BombColourType.Default || bombColourType == Bomb.BombColourType.Silver
                                      || wormSection.ParentWorm.tailColourType == Bomb.BombColourType.Silver || bombColourType == wormSection.ParentWorm.tailColourType)
            {
                wormSection.TakeDamage(currentDamage, gameObject.GetInstanceID());
            }
        }

        // black and silver bombs do damage to worm sections of all colours
        //bombs of all other colours do damage to worm sections of all but their own colour... and silver

        //var wormSection = other.GetComponent<WormSection3D>();
        //if (wormSection != null)
        //{
        //    if (bombColourType == Bomb.BombColourType.Default || bombColourType == Bomb.BombColourType.Silver
        //                          || (bombColourType != wormSection.ParentWorm.tailColourType
        //                                    && wormSection.ParentWorm.tailColourType != Bomb.BombColourType.Silver))
        //        wormSection.TakeDamage(currentDamage, gameObject.GetInstanceID());
        //}
    }

    public void SetBombColour(Bomb.BombColourType bombColour)
    {
        bombColourType = bombColour;
    }

    private void OnDrawGizmos()
    {
        if (forceTrigger != null)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, forceTrigger.radius);
        }
    }
}

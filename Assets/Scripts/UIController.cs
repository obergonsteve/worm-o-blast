using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(AudioSource))]

public class UIController : MonoBehaviour
{
    public TextMeshProUGUI bombsLeftText;                   // in hand
    public TextMeshProUGUI bombsUsedText;                   // placed by hand
    public TextMeshProUGUI bombsScoreText;                  // sections scattered (/ bombs used?)
    public TextMeshProUGUI bombsScatteredText;              // bombs from scattered worm sections
    public TextMeshProUGUI bombCountdownText;               // reduced by spawned worm sections
    public TextMeshProUGUI silverCountdownText;             // reduced by spawned worm sections

    private int bombCountdownWarning = 10;                  // to hilight last few counts
    public AudioClip countDownWarningAudio;
    private bool countDownWarningSounded = false;

    //public TextMeshProUGUI bombsGainedText;
    public TextMeshProUGUI bombCooldownTimeText;
    public Image bombImage;
    public Color bombColour;
    public Color bombCooldownColour;

    public TextMeshProUGUI wormSectionsAliveText;       // sections not yet 'dead'
    public TextMeshProUGUI bombsAliveText;              // bombs from alive worm sections

    public TextMeshProUGUI wormSectionsDeadText;        // sections waiting to be scattered
    public TextMeshProUGUI bombsDeadText;               // bombs from dead frozen) worm sections

    public TextMeshProUGUI wormSectionsInHandText;      // scattered worm sections in hand

    public Image StartPanel;
    public TextMeshProUGUI gameTitleText;
    private float gameTitleTime = .5f;               // scale in time
    private float splatTime = 0.15f;               // worm image
    public Image gameTitleImage;
    private float wormFadeDelay = 0.5f;
    //private float wormSlidePointY = -300;
    private float wormFadeTime = 3f;
    private float wormSplatScale = 1.5f;
    private float introScale = 1.25f;

    private List<int> tweenIds = new List<int>();       // cancelled when game starts (when player taps)

    public AudioClip startAudio;
    public AudioClip mostBlastedAudio;
    public AudioClip wormSplatAudio;

    public TextMeshProUGUI BombTheWormsText;
    public string bombTheWorms = "Tap to Bomb the Worms!";
    public TextMeshProUGUI MostBlastedWinsText;
    public string mostBlastedWins = "Most Blasted Wins!";

    //private float typewriterInterval = 0.125f;
    //public List<AudioClip> typeAudio = new List<AudioClip>();
    //private AudioClip RandomTypeAudio => typeAudio[Random.Range(0, typeAudio.Count)];
    //private Coroutine typeCoroutine;

    public Image GameOverPanel;
    public Button PlayAgainButton;

    private float pulseScale = 1.3f;
    private float pulseTime = 0.2f;

    private Vector3 wormScale;
    private Vector3 titleScale;

    private AudioSource audioSource;


    private void OnEnable()
    {
        GameEvents.OnTouchOn += OnTouchOn;

        GameEvents.OnBombsLeftUpdated += OnBombsLeftUpdated;
        //GameEvents.OnBombsGainedUpdated += OnBombsGainedUpdated;
        GameEvents.OnBombCountdownChanged += OnBombCountdownChanged;
        GameEvents.OnBombCountdownExpired += OnBombCountdownExpired;

        GameEvents.OnSilverCountdownChanged += OnSilverCountdownChanged;

        GameEvents.OnWormSectionsUpdated += OnWormSectionsUpdated;

        GameEvents.OnBombCooldown += OnBombCooldown;
        GameEvents.OnBombCooldownTime += OnBombCooldownTime;

        GameEvents.OnGameOver += OnGameOver;

        PlayAgainButton.onClick.AddListener(OnPlayAgain);
    }

    private void OnDisable()
    {
        GameEvents.OnTouchOn -= OnTouchOn;

        GameEvents.OnBombsLeftUpdated -= OnBombsLeftUpdated;
        //GameEvents.OnBombsGainedUpdated -= OnBombsGainedUpdated;
        GameEvents.OnBombCountdownChanged -= OnBombCountdownChanged;
        GameEvents.OnBombCountdownExpired -= OnBombCountdownExpired;

        GameEvents.OnSilverCountdownChanged -= OnSilverCountdownChanged;

        GameEvents.OnWormSectionsUpdated -= OnWormSectionsUpdated;

        GameEvents.OnBombCooldown -= OnBombCooldown;
        GameEvents.OnBombCooldownTime -= OnBombCooldownTime;

        GameEvents.OnGameOver -= OnGameOver;

        PlayAgainButton.onClick.RemoveListener(OnPlayAgain);
    }

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();

        BombTheWormsText.text = "";
        MostBlastedWinsText.text = "";

        countDownWarningSounded = false;

        GameIntro();
    }


    private void OnTouchOn(Vector2 screenPosition, int touchCount)
    {
        if (StartPanel.gameObject.activeSelf)
        {
            StartPanel.gameObject.SetActive(false);
            //StopCoroutine(typeCoroutine);

            foreach (int tweenId in tweenIds)
            {
                LeanTween.cancel(tweenId);
            }
        }

        GameEvents.OnGameStarted?.Invoke();
    }

    private void OnBombsLeftUpdated(int bombsLeft, int bombsUsed)
    {
        bombsLeftText.text = bombsLeft.ToString();
        bombsUsedText.text = "Used:  " + bombsUsed.ToString();
    }

    private void OnBombCountdownChanged(int countdownRemaining, bool reset)
    {
        bombCountdownText.text = "Expiry:  " + countdownRemaining;

        if (reset)
            countDownWarningSounded = false;

        if (countdownRemaining <= bombCountdownWarning && !countDownWarningSounded)
        {
            countDownWarningSounded = true;

            if (countDownWarningAudio != null)
            {
                audioSource.clip = countDownWarningAudio;
                audioSource.Play();
            }

            //PulseImage(bombImage);
            PulseText(bombCountdownText);
        }
    }

    private void OnSilverCountdownChanged(int countdownRemaining)
    {
        silverCountdownText.text = countdownRemaining.ToString();
    }

    private void OnBombCountdownExpired()
    {
        //PulseText(bombsLeftText);
        PulseImage(bombImage);
    }

    //private void OnBombsGainedUpdated(int bombsGained)
    //{
        //bombsGainedText.text = "Gained:  " + bombsGained;
    //}


    private void OnWormSectionsUpdated(int totalWormSectionsAlive, int totalWormSectionsDead, int totalWormSectionsScattered, int wormSectionsInHand, int sectionsPerBomb)
    {
        wormSectionsInHandText.text = wormSectionsInHand + " / " + sectionsPerBomb;

        wormSectionsAliveText.text = "Awake:  " + totalWormSectionsAlive;
        bombsAliveText.text = ((int)(totalWormSectionsAlive / sectionsPerBomb)).ToString();

        wormSectionsDeadText.text = "Dazed:  " + totalWormSectionsDead;
        bombsDeadText.text = ((int)(totalWormSectionsDead / sectionsPerBomb)).ToString();

        bombsScoreText.text = "Blasted:  " + totalWormSectionsScattered;
        bombsScatteredText.text = ((int)(totalWormSectionsScattered / sectionsPerBomb)).ToString();
    }


    private void GameIntro()
    {
        wormScale = gameTitleImage.transform.localScale;
        gameTitleImage.transform.localScale = Vector3.zero;

        titleScale = gameTitleText.transform.localScale;
        gameTitleText.transform.localScale = Vector3.zero;

        StartPanel.gameObject.SetActive(true);

        // scale in game title
        tweenIds.Add(LeanTween.scale(gameTitleText.gameObject, titleScale * introScale, gameTitleTime)
                            .setDelay(gameTitleTime)
                            .setEaseInQuart()
                            .setOnStart(ScaleInWorm)
                            .setOnComplete(() =>
                            {
                                if (startAudio != null)
                                {
                                    audioSource.clip = startAudio;
                                    audioSource.Play();
                                }

                                tweenIds.Add(LeanTween.scale(gameTitleText.gameObject, titleScale, gameTitleTime)
                                            .setEaseOutQuart().id);
                            }).id);
                            
    }

    private void ScaleInWorm()
    {
        // scale in worm image
        tweenIds.Add(LeanTween.scale(gameTitleImage.gameObject, wormScale, gameTitleTime / 2f)
                    .setDelay(gameTitleTime * 2f)
                    .setEaseInQuad()
                    .setOnComplete(() =>
                    {
                        SplatWorm();
                    })
                    .id);
    }

    private void SplatWorm()
    {
        if (wormSplatAudio != null)
        {
            audioSource.clip = wormSplatAudio;
            audioSource.Play();
        }

        //var wormScale = gameTitleImage.transform.localScale;

        tweenIds.Add(LeanTween.scale(gameTitleImage.gameObject, wormScale * wormSplatScale, splatTime)
                    //.setDelay(gameTitleTime / 2f)
                    .setEaseOutQuart()
                    .setOnComplete(() =>
                    {
                        //SlideDownWorm();
                        FadeWorm();
                        ScaleInBombTheWorms();

                        tweenIds.Add(LeanTween.scale(gameTitleImage.gameObject, wormScale, splatTime * 2f)
                                    //.setDelay(wormFadeDelay)
                                    .setEaseInQuart()
                                    .id);
                    }).id);
    }


    //private void SlideDownWorm()
    //{
    //    tweenIds.Add(LeanTween.moveY(gameTitleImage.gameObject, wormSlidePointY, wormSlideTime)
    //                .setDelay(wormFadeDelay)
    //                .setEaseInQuart()
    //                .setOnStart(() =>
    //                {
    //                    FadeWorm();
    //                    ScaleInBombTheWorms();
    //                })
    //                .id);
    //}

    private void FadeWorm()
    {
        tweenIds.Add(LeanTween.value(gameTitleImage.gameObject, Color.white, Color.clear, wormFadeTime)
                    .setDelay(wormFadeDelay)
                    .setEaseInQuad()
                    .setOnUpdate((Color c) =>
                    {
                        gameTitleImage.color = c;
                    })
                    .id);

        tweenIds.Add(LeanTween.scale(gameTitleImage.gameObject, Vector3.zero, wormFadeTime)
                    .setDelay(wormFadeDelay)
                    .setEaseInQuad()
                    .id);
    }

    private void ScaleInBombTheWorms()
    {
        BombTheWormsText.transform.localScale = Vector2.zero;
        BombTheWormsText.text = bombTheWorms;

        tweenIds.Add(LeanTween.scale(BombTheWormsText.gameObject, Vector2.one, gameTitleTime)
                    .setEaseOutBounce()
                    .setOnComplete(() =>
                    {
                        MostBlastedWinsText.text = mostBlastedWins;
                        PulseText(MostBlastedWinsText);

                        if (mostBlastedAudio != null)
                        {
                            audioSource.clip = mostBlastedAudio;
                            audioSource.Play();
                        }
                    })
                    .id);
    }


    //private IEnumerator TypeText(string text, TextMeshProUGUI display, float speed = 1f)
    //{
    //    string textToDisplay = "";
    //    display.text = "";

    //    for (int i = 0; i < text.Length; i++)
    //    {
    //        textToDisplay += text[i];
    //        display.text = textToDisplay;

    //        if (text[i] == ' ')
    //            yield return null;
    //        else
    //        {
    //            if (typeAudio != null)
    //            {
    //                audioSource.clip = RandomTypeAudio;
    //                audioSource.Play();
    //            }

    //            yield return new WaitForSecondsRealtime(typewriterInterval * speed);
    //        }
    //    }
    //}

    private void OnBombCooldown(bool coolingDown)
    {
        bombCooldownTimeText.enabled = coolingDown;
        bombImage.color = coolingDown ? bombCooldownColour : bombColour;
    }

    private void OnBombCooldownTime(int timeRemaining)
    {
        bombCooldownTimeText.text = timeRemaining.ToString();

        PulseText(bombCooldownTimeText);
    }

    private void PulseText(TextMeshProUGUI text)
    {
        var startScale = text.transform.localScale;

        LeanTween.scale(text.gameObject, startScale * pulseScale, pulseTime)
                            .setEaseOutElastic()
                            .setOnComplete(() =>
                             {
                                LeanTween.scale(text.gameObject, startScale, pulseTime)
                                            .setEaseOutQuart();
                             });
    }

    private void PulseImage(Image image)
    {
        var startScale = image.transform.localScale;

        LeanTween.scale(image.gameObject, startScale * pulseScale, pulseTime)
                            .setEaseOutElastic()
                            .setOnComplete(() =>
                            {
                                LeanTween.scale(image.gameObject, startScale, pulseTime)
                                        .setEaseOutQuart();
                            });
    }


    private void OnGameOver(int finalScore)
    {
        GameOverPanel.gameObject.SetActive(true);
    }

    private void OnPlayAgain()
    {
        var currentScene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(currentScene.name); 
    }
}

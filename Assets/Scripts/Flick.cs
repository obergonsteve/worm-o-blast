using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class to enable an object to be dragged and thrown
/// in 3D space via touch / mouse.
/// Object follows raycast from touch screen position.
/// </summary>

[RequireComponent(typeof(Rigidbody))]
public class Flick : MonoBehaviour
{
    private bool activated = false;     // ie. 'grabbed' by raycast

    //[SerializeField]
    private float draggingSpeed = 20f;     //

    private float maxDragDistance = 8f;       // automatically thrown if exceeded

    private float maxVelocity = 40f;     // clamped

    [SerializeField]
    private bool usePhysicsForThrow = false;
    private float throwTime = 0.5f;      // time to reach destination if not physics
    private float throwLiftY = 0.5f;      // lift speed if not physics
    private float maxThrowLiftY = 12f;      // to limit height

    [SerializeField]
    private float throwSpeedY = 1f;     // vertical lift for throw (if physics)

    private Vector3 activationPosition;
    private bool hittingThrowTarget = false;     // is object hitting target trigger?  (eg. water)

    private float disappearTime = 0.5f;      // scale to zero
    private float snapTime = 0.1f;           // move back to start

    private int throwTweenX;
    private int throwTweenY;
    private int throwTweenZ;

    private Rigidbody rb;


    public void Start()
    {
        rb = GetComponent<Rigidbody>();

        rb.isKinematic = !usePhysicsForThrow;
    }

    private void FixedUpdate()
    {
        // limit velocity
        if (usePhysicsForThrow)
        {
            rb.velocity = Vector3.ClampMagnitude(rb.velocity, maxVelocity);
        }
    }

    // on touch on
    public void Activate()
    {
        if (activated)
            return;

        activated = true;
        activationPosition = transform.position;

        // TODO: trigger animation?  play audio?

        // fire activated event
        GameEvents.OnFlickableActivated?.Invoke(this);
    }

    // on touch move
    // released == true if touch/mouse released
    public void Drag(Vector3 toPosition)
    {
        if (!activated)
            return;

        // fire dragged event
        GameEvents.OnFlickableDragged?.Invoke(this);

        if ((toPosition - activationPosition).magnitude > maxDragDistance)
        {
            Throw(toPosition);
        }
        else
        {
            // maintain original y position
            // move a step closer to toPosition
            float step = draggingSpeed * Time.deltaTime; // calculate distance to move
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(toPosition.x, activationPosition.y, toPosition.z), step);
        }

        // TODO: trigger animation?  play audio?
    }

    // on touch off
    public void Throw(Vector3 hitPosition)
    {
        if (!activated)
            return;

        // throw to starting y position
        Vector3 throwTarget = new Vector3(hitPosition.x, activationPosition.y, hitPosition.z);
        Vector3 throwDirection = throwTarget - transform.position;        // represents direction and speed

        if (usePhysicsForThrow)
        {
            // lift force in throwDirection
            var verticalLift = throwSpeedY * throwDirection.magnitude;

            // greater force applied with greater distance between transform.position and toPosition
            var throwForce = new Vector3(throwDirection.x, verticalLift, throwDirection.z);

            GameEvents.OnFlickableThrown?.Invoke(this, throwForce);
            activated = false;
        }
        else            // tween x, y (lift) and z axes separately to simulate arc / gravity
        {
            var throwY = throwLiftY * throwDirection.magnitude;     // higher lift for greater distance
            throwY = Mathf.Clamp(throwY, 0f, maxThrowLiftY);

            // lift (y)
            throwTweenY = LeanTween.moveY(gameObject, throwY, throwTime / 2f)
                        .setEaseInOutQuad()
                        .setOnComplete(() =>
                        {
                            // back to start y
                            DropToStartY();
                        }).id;

            // move z
            throwTweenZ = LeanTween.moveZ(gameObject, throwTarget.z, throwTime)
                        .setEaseLinear().id;
            //.setEaseInSine().id;
            //.setEaseOutSine().id;


            // move x
            throwTweenX = LeanTween.moveX(gameObject, throwTarget.x, throwTime)
                        .setEaseLinear()
                        //.setEaseOutSine()
                        //.setEaseInSine()
                        .setOnComplete(() =>
                        {
                            activated = false;
                        }).id;
        }

        // TODO: trigger animation?  play audio?
    }

    private void StopThrow()
    {
        LeanTween.cancel(throwTweenX);
        LeanTween.cancel(throwTweenY);
        LeanTween.cancel(throwTweenZ);

        activated = false;
    }

    private void DropToStartY()
    {
        // back to start y
        throwTweenY = LeanTween.moveY(gameObject, activationPosition.y, throwTime / 2f)
                    .setEaseInQuad().id;
    }

    private void Disappear()
    {
        LeanTween.scale(gameObject, Vector3.zero, disappearTime)
                    .setEaseOutQuad();
    }

    private void SnapBackToStart()
    {
        LeanTween.move(gameObject, activationPosition, snapTime)
                        .setEaseOutQuad();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!usePhysicsForThrow)
        {
            StopThrow();        // cancel tweens
            DropToStartY();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("ThrowTarget"))
        {
            hittingThrowTarget = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("ThrowTarget"))
        {
            hittingThrowTarget = false;
        }
    }


    public void Release()
    {
        if (hittingThrowTarget)
        {
            Disappear();
        }
        else
        {
            SnapBackToStart();
        }
    }
}
